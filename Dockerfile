FROM maven:3.8.4-openjdk-17 AS builder

WORKDIR /appUni
COPY . /appUni

RUN mvn clean install -DskipTests -DskipSpotlessApply=true

FROM openjdk:17
VOLUME /tmp
EXPOSE 8080

WORKDIR /appUni
COPY --from=builder /appUni/target/university-1.0-SNAPSHOT.jar appUni.jar
ENTRYPOINT ["java","-jar","appUni.jar"]
