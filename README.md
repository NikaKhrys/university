# University

### Project description
This is a web application that allows the university to manage lecture schedules for students, groups and teachers. It's also connected with an Accountancy application that allows user to keep track of money transactions within the university.

### Run application
You can run a docker container that will start both University and Accountancy apps with connected databases with the command:
`$env:ACCOUNTANCY_DB_URL="jdbc:postgresql://accountancy-db/accountancy";$env:UNIVERSITY_DB_URL="jdbc:postgresql://university-db/university";$env:TRANSACTIONS_URL="http://accountancy-app:8081/transactions/"; docker-compose up;`

You can ensure that the application has started successfully with the following links:
- [http://host:8080/swagger-ui/index.html](http://host:8081/swagger-ui/index.html)
- [http://host:8080/api-docs](http://host:8081/api-docs)

If you want to access application endpoints, use admin credentials:  
username: bobmartin  
password: BobPass