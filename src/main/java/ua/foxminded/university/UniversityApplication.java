/* (C)2023 */
package ua.foxminded.university;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class UniversityApplication {

  public static void main(String[] args) {
    SpringApplication.run(UniversityApplication.class, args);
  }
}
