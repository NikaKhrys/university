/* (C)2024 */
package ua.foxminded.university.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Positive;
import java.time.LocalDate;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.service.dto.LessonDto;

/**
 * A simple REST controller, which supports end points to manage students' and professors' schedules
 *
 * @author veronika_khrystoforova
 */
@RestController
@RequestMapping("/lessons")
public interface LessonController {
  @Operation(summary = "Get students schedule for day or month")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Lessons found",
            content =
                @Content(
                    mediaType = "application/json",
                    examples =
                        @ExampleObject(
                            """
                                            {
                                              "content": [
                                                {
                                                  "date": "2024-01-01",
                                                  "time": "08:30:00",
                                                  "auditorium": {
                                                    "number": 101
                                                  },
                                                  "course": {
                                                    "name": "Math"
                                                  },
                                                  "professor": {
                                                    "firstName": "Bob",
                                                    "lastName": "Martin"
                                                  },
                                                  "groups": [
                                                    {
                                                      "name": "B1"
                                                    },
                                                    {
                                                      "name": "B2"
                                                    }
                                                  ]
                                                }
                                              ],
                                              "pageable": {
                                                "sort": {
                                                  "empty": true,
                                                  "sorted": false,
                                                  "unsorted": true
                                                },
                                                "offset": 0,
                                                "pageNumber": 0,
                                                "pageSize": 20,
                                                "paged": true,
                                                "unpaged": false
                                              },
                                              "last": true,
                                              "totalElements": 1,
                                              "totalPages": 1,
                                              "first": true,
                                              "size": 20,
                                              "number": 0,
                                              "sort": {
                                                "empty": true,
                                                "sorted": false,
                                                "unsorted": true
                                              },
                                              "numberOfElements": 1,
                                              "empty": false
                                            }
                                            """))),
        @ApiResponse(responseCode = "204", description = "No lessons on page", content = @Content),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request parameters",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json"))
      })
  @GetMapping("/student/{id}")
  ResponseEntity<Page<LessonDto>> getStudentSchedule(
      @Parameter(description = "id of the student whose schedule is searched", example = "1")
          @PathVariable(name = "id")
          Integer id,
      @Parameter(description = "date of searched schedule", example = "2024-01-01")
          @RequestParam(name = "date", required = false)
          LocalDate date,
      @Parameter(description = "month of searched schedule", example = "1")
          @RequestParam(name = "month", required = false)
          @Positive
          @Max(12)
          Integer month,
      @ParameterObject Pageable pageable);

  @Operation(summary = "Get professors schedule for day or month")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Lessons found",
            content =
                @Content(
                    mediaType = "application/json",
                    examples =
                        @ExampleObject(
                            """
                                            {
                                              "content": [
                                                {
                                                  "date": "2024-01-04",
                                                  "time": "12:30:00",
                                                  "auditorium": {
                                                    "number": 103
                                                  },
                                                  "course": {
                                                    "name": "Logistics"
                                                  },
                                                  "professor": {
                                                    "firstName": "Bob",
                                                    "lastName": "Martin"
                                                  },
                                                  "groups": [
                                                    {
                                                      "name": "B2"
                                                    }
                                                  ]
                                                }
                                              ],
                                              "pageable": {
                                                "sort": {
                                                  "empty": true,
                                                  "sorted": false,
                                                  "unsorted": true
                                                },
                                                "offset": 1,
                                                "pageNumber": 1,
                                                "pageSize": 1,
                                                "paged": true,
                                                "unpaged": false
                                              },
                                              "last": false,
                                              "totalElements": 7,
                                              "totalPages": 7,
                                              "first": false,
                                              "size": 1,
                                              "number": 1,
                                              "sort": {
                                                "empty": true,
                                                "sorted": false,
                                                "unsorted": true
                                              },
                                              "numberOfElements": 1,
                                              "empty": false
                                            }
                                            """))),
        @ApiResponse(responseCode = "204", description = "No lessons on page", content = @Content),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request parameters",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json"))
      })
  @GetMapping("/professor/{id}")
  ResponseEntity<Page<LessonDto>> getProfessorSchedule(
      @Parameter(description = "id of professor whose schedule is searched", example = "7")
          @PathVariable(name = "id")
          Integer id,
      @Parameter(description = "date of searched schedule", example = "2024-01-01")
          @RequestParam(name = "date", required = false)
          LocalDate date,
      @Parameter(description = "month of searched schedule", example = "1")
          @RequestParam(name = "month", required = false)
          @Positive
          @Max(12)
          Integer month,
      @ParameterObject Pageable pageable);
}
