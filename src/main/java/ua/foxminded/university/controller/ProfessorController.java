/* (C)2024 */
package ua.foxminded.university.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.service.dto.ProfessorDto;

/**
 * A simple REST controller, which supports end points to manage professor users
 *
 * @author veronika_khrystoforova
 */
@RestController
@RequestMapping("/professors")
public interface ProfessorController {
  String FIND_ALL_PROFESSORS_EXAMPLE =
      """
                                            {
                                              "content": [
                                                {
                                                  "id": 7,
                                                  "firstName": "Bob",
                                                  "lastName": "Martin",
                                                  "info": {
                                                    "id": 7,
                                                    "phoneNumber": "380123456789",
                                                    "email": "emailnew@gmail.com",
                                                    "address": "address7"
                                                  },
                                                  "coursesId": [
                                                    1,
                                                    2
                                                  ]
                                                }
                                              ],
                                              "pageable": {
                                                "sort": {
                                                  "empty": true,
                                                  "sorted": false,
                                                  "unsorted": true
                                                },
                                                "offset": 1,
                                                "pageNumber": 1,
                                                "pageSize": 1,
                                                "paged": true,
                                                "unpaged": false
                                              },
                                              "last": true,
                                              "totalElements": 2,
                                              "totalPages": 2,
                                              "first": false,
                                              "size": 1,
                                              "number": 1,
                                              "sort": {
                                                "empty": true,
                                                "sorted": false,
                                                "unsorted": true
                                              },
                                              "numberOfElements": 1,
                                              "empty": false
                                            }
                                            """;
  String FIND_PROFESSOR_BY_ID_EXAMPLE =
      """
                                            {
                                              "id": 7,
                                              "firstName": "Bob",
                                              "lastName": "Martin",
                                              "info": {
                                                "id": 7,
                                                "phoneNumber": "380777777777",
                                                "email": "email7@gmail.com",
                                                "address": "address7"
                                              },
                                              "coursesId": [
                                                1,
                                                2,
                                                3
                                              ]
                                            }
                                            """;
  String SAVE_PROFESSOR_EXAMPLE =
      """
                                            {
                                              "id": 10,
                                              "firstName": "New",
                                              "lastName": "Professor",
                                              "info": {
                                                "id": 11,
                                                "phoneNumber": "380123456789",
                                                "email": "emailnew@gmail.com",
                                                "address": "address new"
                                              },
                                              "coursesId": [
                                                1,
                                                2
                                              ]
                                            }
                                            """;
  String UPDATE_PROFESSOR_EXAMPLE =
      """
                                            {
                                              "id": 7,
                                              "firstName": "Bob",
                                              "lastName": "Martin",
                                              "info": {
                                                "id": 7,
                                                "phoneNumber": "380123456789",
                                                "email": "emailnew@gmail.com",
                                                "address": "address7"
                                              },
                                              "coursesId": [
                                                1,
                                                2
                                              ]
                                            }
                                            """;

  @Operation(summary = "Get all professors")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found all professors",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(FIND_ALL_PROFESSORS_EXAMPLE))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request parameters",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "204",
            description = "No professors on page",
            content = @Content)
      })
  @GetMapping
  ResponseEntity<Page<ProfessorDto>> findAll(@ParameterObject Pageable pageable);

  @Operation(summary = "Get professor by its id")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Professor found",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(FIND_PROFESSOR_BY_ID_EXAMPLE))),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(responseCode = "404", description = "Professor not found", content = @Content)
      })
  @GetMapping("/{id}")
  ResponseEntity<ProfessorDto> findById(
      @Parameter(description = "id of the professor to be searched", example = "7")
          @PathVariable(name = "id")
          Integer id);

  @Operation(summary = "Create new professor")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "New professor created",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(SAVE_PROFESSOR_EXAMPLE))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request body",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json"))
      })
  @PostMapping
  ResponseEntity<ProfessorDto> save(
      @RequestBody @Valid ProfessorDto professorDto, Authentication authentication);

  @Operation(summary = "Update professor by its id")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Professor updated",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(UPDATE_PROFESSOR_EXAMPLE))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request body",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "404",
            description = "Professor not found",
            content = @Content(mediaType = "application/json"))
      })
  @PutMapping("/{id}")
  ResponseEntity<ProfessorDto> update(
      @Parameter(description = "id of professor to be updated", example = "7")
          @PathVariable(name = "id")
          Integer id,
      @RequestBody @Valid ProfessorDto professorDto,
      Authentication authentication);

  @Operation(summary = "Delete professor by its id")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "Professor deleted", content = @Content),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "404",
            description = "Professor not found",
            content = @Content(mediaType = "application/json"))
      })
  @DeleteMapping("/{id}")
  ResponseEntity<Void> deleteById(
      @Parameter(description = "id of professor to be deleted", example = "8")
          @PathVariable(name = "id")
          Integer id);
}
