/* (C)2024 */
package ua.foxminded.university.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.service.dto.StudentDto;

/**
 * A simple REST controller, which supports end points to manage student users
 *
 * @author veronika_khrystoforova
 */
@RestController
@RequestMapping("/students")
public interface StudentController {
  String FIND_ALL_STUDENTS_EXAMPLE =
      """
                                            {
                                               "content": [
                                                 {
                                                   "id": 5,
                                                   "firstName": "Jack",
                                                   "lastName": "Hill",
                                                   "info": {
                                                     "id": 5,
                                                     "phoneNumber": "380555555555",
                                                     "email": "email5@gmail.com",
                                                     "address": "address5"
                                                   },
                                                   "facultyId": 1,
                                                   "year": 2,
                                                   "groupId": 2
                                                 }
                                               ],
                                               "pageable": {
                                                 "sort": {
                                                   "empty": true,
                                                   "sorted": false,
                                                   "unsorted": true
                                                 },
                                                 "offset": 3,
                                                 "pageNumber": 3,
                                                 "pageSize": 1,
                                                 "paged": true,
                                                 "unpaged": false
                                               },
                                               "last": false,
                                               "totalElements": 6,
                                               "totalPages": 6,
                                               "first": false,
                                               "size": 1,
                                               "number": 3,
                                               "sort": {
                                                 "empty": true,
                                                 "sorted": false,
                                                 "unsorted": true
                                               },
                                               "numberOfElements": 1,
                                               "empty": false
                                             }
                                            """;

  String FIND_STUDENT_BY_ID_EXAMPLE =
      """
                                    {
                                      "id": 1,
                                      "firstName": "Milly",
                                      "lastName": "Smith",
                                      "info": {
                                        "id": 1,
                                        "phoneNumber": "380650656565",
                                        "email": "email1@gmail.com",
                                        "address": "address1"
                                      },
                                      "facultyId": 1,
                                      "year": 1,
                                      "groupId": 1
                                    }
                                    """;
  String SAVE_STUDENT_EXAMPLE =
      """
                                            {
                                               "id": 9,
                                               "firstName": "New",
                                               "lastName": "Student",
                                               "info": {
                                                 "id": 9,
                                                 "phoneNumber": "380123456789",
                                                 "email": "newemail@gmail.com",
                                                 "address": "new address"
                                               },
                                               "facultyId": 1,
                                               "year": 4,
                                               "groupId": 1
                                             }
                                            """;
  String UPDATE_STUDENT_EXAMPLE =
      """
                                            {
                                              "id": 1,
                                              "firstName": "Milly",
                                              "lastName": "Smith",
                                              "info": {
                                                "id": 1,
                                                "phoneNumber": "380123456789",
                                                "email": "emailnew@gmail.com",
                                                "address": "address new"
                                              },
                                              "facultyId": 1,
                                              "year": 1,
                                              "groupId": 1
                                            }
                                            """;

  @Operation(summary = "Get all students")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Found all students",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(FIND_ALL_STUDENTS_EXAMPLE))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request parameters",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(responseCode = "204", description = "No students on page", content = @Content)
      })
  @GetMapping
  ResponseEntity<Page<StudentDto>> findAll(@ParameterObject Pageable pageable);

  @Operation(summary = "Get student by its id")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Student found",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(FIND_STUDENT_BY_ID_EXAMPLE))),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(responseCode = "404", description = "Student not found", content = @Content)
      })
  @GetMapping("/{id}")
  ResponseEntity<StudentDto> findById(
      @Parameter(description = "id of the student to be searched", example = "1")
          @PathVariable(name = "id")
          Integer id);

  @Operation(summary = "Create new student")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "201",
            description = "New student created",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(SAVE_STUDENT_EXAMPLE))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request body",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json"))
      })
  @PostMapping
  ResponseEntity<StudentDto> save(
      @RequestBody @Valid StudentDto studentDto, Authentication authentication);

  @Operation(summary = "Update student by its id")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Student updated",
            content =
                @Content(
                    mediaType = "application/json",
                    examples = @ExampleObject(UPDATE_STUDENT_EXAMPLE))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid request body",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "404",
            description = "Student not found",
            content = @Content(mediaType = "application/json"))
      })
  @PutMapping("/{id}")
  ResponseEntity<StudentDto> update(
      @Parameter(description = "id of student to be updated", example = "1")
          @PathVariable(name = "id")
          Integer id,
      @RequestBody @Valid StudentDto studentDto,
      Authentication authentication);

  @Operation(summary = "Delete student by its id")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "Student deleted", content = @Content),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "404",
            description = "Student not found",
            content = @Content(mediaType = "application/json"))
      })
  @DeleteMapping("/{id}")
  ResponseEntity<Void> deleteById(
      @Parameter(description = "id of student to be deleted", example = "3")
          @PathVariable(name = "id")
          Integer id);
}
