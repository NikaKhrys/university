/* (C)2024 */
package ua.foxminded.university.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.time.LocalDate;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.service.dto.TransactionDto;

/**
 * A simple REST controller, which supports end points to manage students' and professors' currency
 * transactions
 *
 * @author veronika_khrystoforova
 */
@RestController
@RequestMapping("transactions")
public interface TransactionController {
  @Operation(summary = "Get persons transactions by their id")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Transactions found",
            content =
                @Content(
                    mediaType = "application/json",
                    examples =
                        @ExampleObject(
                            """
                                            {
                                              "content": [
                                                {
                                                  "id": 1,
                                                  "personId": 1,
                                                  "position": "STUDENT",
                                                  "currency": "USD",
                                                  "value": 70,
                                                  "date": "2023-01-01",
                                                  "purpose": "SCHOLARSHIP"
                                                }
                                              ],
                                              "pageable": {
                                                "sort": {
                                                  "empty": true,
                                                  "sorted": false,
                                                  "unsorted": true
                                                },
                                                "offset": 0,
                                                "pageNumber": 0,
                                                "pageSize": 1,
                                                "paged": true,
                                                "unpaged": false
                                              },
                                              "last": false,
                                              "totalElements": 3,
                                              "totalPages": 3,
                                              "first": true,
                                              "size": 1,
                                              "number": 0,
                                              "sort": {
                                                "empty": true,
                                                "sorted": false,
                                                "unsorted": true
                                              },
                                              "numberOfElements": 1,
                                              "empty": false
                                            }
                                            """))),
        @ApiResponse(
            responseCode = "204",
            description = "No transactions",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "400",
            description = "Bad request",
            content = @Content(mediaType = "application/json")),
        @ApiResponse(
            responseCode = "401",
            description = "Access denied",
            content = @Content(mediaType = "application/json"))
      })
  @GetMapping("/{id}")
  ResponseEntity<Page<TransactionDto>> getTransactionsInCurrency(
      @Parameter(description = "id of the person", example = "1") @PathVariable(name = "id")
          Integer personId,
      @ParameterObject Pageable pageable,
      @Parameter(description = "date from which transactions search starts", example = "2023-01-01")
          @RequestParam(name = "startDate")
          LocalDate startDate,
      @Parameter(description = "date to which transactions search ends", example = "2023-03-01")
          @RequestParam(name = "endDate")
          LocalDate endDate,
      @Parameter(
              description = "currency in which transactions values should be represented",
              example = "USD")
          @RequestParam(name = "currency")
          String currency);
}
