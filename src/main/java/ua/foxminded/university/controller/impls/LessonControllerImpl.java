/* (C)2023-2024 */
package ua.foxminded.university.controller.impls;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Positive;
import java.time.LocalDate;
import java.time.YearMonth;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.controller.LessonController;
import ua.foxminded.university.service.LessonService;
import ua.foxminded.university.service.dto.LessonDto;

@RestController
@RequestMapping("/lessons")
@Validated
public class LessonControllerImpl implements LessonController {
  private final LessonService service;

  @Autowired
  public LessonControllerImpl(LessonService service) {
    this.service = service;
  }

  @PreAuthorize(
      "hasAuthority('ADMIN') or @securityChecker.checkIfRecordIsStudents(authentication,#id)")
  @GetMapping("/student/{id}")
  public ResponseEntity<Page<LessonDto>> getStudentSchedule(
      @PathVariable(name = "id") Integer id,
      @RequestParam(name = "date", required = false) LocalDate date,
      @RequestParam(name = "month", required = false) @Positive @Max(12) Integer month,
      @ParameterObject Pageable pageable) {
    if (validateTimeRequestParameters(date, month))
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    if (month != null) return getStudentScheduleForMonth(id, month, pageable);
    return getStudentScheduleForDay(id, date, pageable);
  }

  @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('PROFESSOR')")
  @GetMapping("/professor/{id}")
  public ResponseEntity<Page<LessonDto>> getProfessorSchedule(
      @PathVariable(name = "id") Integer id,
      @RequestParam(name = "date", required = false) LocalDate date,
      @RequestParam(name = "month", required = false) @Positive @Max(12) Integer month,
      @ParameterObject Pageable pageable) {
    if (validateTimeRequestParameters(date, month))
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    if (month != null) return getProfessorScheduleForMonth(id, month, pageable);
    return getProfessorScheduleForDay(id, date, pageable);
  }

  private boolean validateTimeRequestParameters(LocalDate date, Integer month) {
    if (date == null && month == null) return true;
    return date != null && month != null;
  }

  private ResponseEntity<Page<LessonDto>> getStudentScheduleForDay(
      Integer id, LocalDate date, Pageable pageable) {
    Page<LessonDto> lessons = service.getStudentLessonsForDate(id, date, pageable);

    if (lessons.isEmpty()) return getEmptyPageResponse();
    return new ResponseEntity<>(lessons, HttpStatus.OK);
  }

  private ResponseEntity<Page<LessonDto>> getStudentScheduleForMonth(
      Integer id, Integer month, Pageable pageable) {
    YearMonth yearMonth = YearMonth.now().withMonth(month);
    Page<LessonDto> lessons = service.getStudentLessonsForMonth(id, yearMonth, pageable);

    if (lessons.isEmpty()) return getEmptyPageResponse();
    return new ResponseEntity<>(lessons, HttpStatus.OK);
  }

  private ResponseEntity<Page<LessonDto>> getProfessorScheduleForDay(
      Integer id, LocalDate date, Pageable pageable) {
    Page<LessonDto> lessons = service.getProfessorLessonsForDate(id, date, pageable);

    if (lessons.isEmpty()) return getEmptyPageResponse();
    return new ResponseEntity<>(lessons, HttpStatus.OK);
  }

  private ResponseEntity<Page<LessonDto>> getProfessorScheduleForMonth(
      Integer id, Integer month, Pageable pageable) {
    YearMonth yearMonth = YearMonth.now().withMonth(month);
    Page<LessonDto> lessons = service.getProfessorLessonsForMonth(id, yearMonth, pageable);

    if (lessons.isEmpty()) return getEmptyPageResponse();
    return new ResponseEntity<>(lessons, HttpStatus.OK);
  }

  private ResponseEntity<Page<LessonDto>> getEmptyPageResponse() {
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
