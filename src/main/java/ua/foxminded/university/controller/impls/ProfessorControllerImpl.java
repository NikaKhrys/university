/* (C)2023-2024 */
package ua.foxminded.university.controller.impls;

import jakarta.validation.Valid;
import java.util.Objects;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.controller.ProfessorController;
import ua.foxminded.university.service.ProfessorService;
import ua.foxminded.university.service.dto.ProfessorDto;

@RestController
@RequestMapping("/professors")
@Validated
public class ProfessorControllerImpl implements ProfessorController {
  private final ProfessorService service;

  @Autowired
  public ProfessorControllerImpl(ProfessorService service) {
    this.service = service;
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @GetMapping
  public ResponseEntity<Page<ProfessorDto>> findAll(@ParameterObject Pageable pageable) {
    Page<ProfessorDto> professors = service.findAll(pageable);

    if (professors.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    return new ResponseEntity<>(professors, HttpStatus.OK);
  }

  @PreAuthorize(
      """
        hasAuthority('ADMIN')
        or @securityChecker.checkIfRecordIsProfessors(authentication,#id)""")
  @GetMapping("/{id}")
  public ResponseEntity<ProfessorDto> findById(@PathVariable(name = "id") Integer id) {
    ProfessorDto professor = service.findById(id);
    return new ResponseEntity<>(professor, HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<ProfessorDto> save(
      @RequestBody @Valid ProfessorDto professorDto, Authentication authentication) {
    String username = Objects.isNull(authentication) ? null : authentication.getName();
    ProfessorDto professor = service.save(professorDto, username);
    return new ResponseEntity<>(professor, HttpStatus.CREATED);
  }

  @PreAuthorize(
      """
        hasAuthority('ADMIN')
        or @securityChecker.checkIfRecordIsProfessors(authentication,#id)""")
  @PutMapping("/{id}")
  public ResponseEntity<ProfessorDto> update(
      @PathVariable(name = "id") Integer id,
      @RequestBody @Valid ProfessorDto professorDto,
      Authentication authentication) {
    String username = Objects.isNull(authentication) ? null : authentication.getName();
    ProfessorDto professor = service.update(id, professorDto, username);
    return new ResponseEntity<>(professor, HttpStatus.OK);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteById(@PathVariable(name = "id") Integer id) {
    service.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
