/* (C)2023-2024 */
package ua.foxminded.university.controller.impls;

import jakarta.validation.Valid;
import java.util.Objects;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.foxminded.university.controller.StudentController;
import ua.foxminded.university.service.StudentService;
import ua.foxminded.university.service.dto.StudentDto;

@RestController
@RequestMapping("/students")
@Validated
public class StudentControllerImpl implements StudentController {
  private final StudentService service;

  @Autowired
  public StudentControllerImpl(StudentService service) {
    this.service = service;
  }

  @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('PROFESSOR')")
  @GetMapping
  public ResponseEntity<Page<StudentDto>> findAll(@ParameterObject Pageable pageable) {
    Page<StudentDto> students = service.findAll(pageable);

    if (students.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    return new ResponseEntity<>(students, HttpStatus.OK);
  }

  @PreAuthorize(
      """
        hasAuthority('ADMIN') or hasAuthority('PROFESSOR')
        or @securityChecker.checkIfRecordIsStudents(authentication,#id)""")
  @GetMapping("/{id}")
  public ResponseEntity<StudentDto> findById(@PathVariable(name = "id") Integer id) {
    StudentDto student = service.findById(id);
    return new ResponseEntity<>(student, HttpStatus.OK);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @PostMapping
  public ResponseEntity<StudentDto> save(
      @RequestBody @Valid StudentDto studentDto, Authentication authentication) {
    String username = Objects.isNull(authentication) ? null : authentication.getName();
    StudentDto student = service.save(studentDto, username);
    return new ResponseEntity<>(student, HttpStatus.CREATED);
  }

  @PreAuthorize(
      """
           hasAuthority('ADMIN')
           or @securityChecker.checkIfRecordIsStudents(authentication,#id)""")
  @PutMapping("/{id}")
  public ResponseEntity<StudentDto> update(
      @PathVariable(name = "id") Integer id,
      @RequestBody @Valid StudentDto studentDto,
      Authentication authentication) {
    String username = Objects.isNull(authentication) ? null : authentication.getName();
    StudentDto student = service.update(id, studentDto, username);
    return new ResponseEntity<>(student, HttpStatus.OK);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteById(@PathVariable(name = "id") Integer id) {
    service.deleteById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
