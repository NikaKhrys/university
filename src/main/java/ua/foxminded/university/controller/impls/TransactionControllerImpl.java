/* (C)2023-2024 */
package ua.foxminded.university.controller.impls;

import java.time.LocalDate;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ua.foxminded.university.controller.TransactionController;
import ua.foxminded.university.service.TransactionService;
import ua.foxminded.university.service.dto.TransactionDto;

@RestController
@RequestMapping("transactions")
public class TransactionControllerImpl implements TransactionController {
  private final TransactionService service;

  @Autowired
  public TransactionControllerImpl(TransactionService service) {
    this.service = service;
  }

  @PreAuthorize(
      """
            hasAuthority('ADMIN') or
            @securityChecker.checkIfTransactionIsPersons(authentication,#personId)""")
  @GetMapping("/{id}")
  public ResponseEntity<Page<TransactionDto>> getTransactionsInCurrency(
      @PathVariable(name = "id") Integer personId,
      @ParameterObject Pageable pageable,
      @RequestParam(name = "startDate") LocalDate startDate,
      @RequestParam(name = "endDate") LocalDate endDate,
      @RequestParam(name = "currency") String currency) {
    Page<TransactionDto> transactions =
        service.getTransactionsInCurrency(personId, startDate, endDate, currency, pageable);
    if (transactions.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    return new ResponseEntity<>(transactions, HttpStatus.OK);
  }
}
