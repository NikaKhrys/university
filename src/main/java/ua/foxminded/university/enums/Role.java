/* (C)2023-2024 */
package ua.foxminded.university.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * Enum with all users' roles
 *
 * @author veronika_khrystoforova
 */
public enum Role implements GrantedAuthority {
  STUDENT,
  PROFESSOR,
  ADMIN;

  @Override
  public String getAuthority() {
    return name();
  }
}
