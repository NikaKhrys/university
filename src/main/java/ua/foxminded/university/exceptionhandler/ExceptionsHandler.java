/* (C)2023-2024 */
package ua.foxminded.university.exceptionhandler;

import jakarta.validation.ConstraintViolationException;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ua.foxminded.university.exceptionhandler.exceptions.CurrencyNotSupportedException;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;

/**
 * ExceptionHandler for services that builds response body for failed requests
 *
 * @author veronika_khrystoforova
 */
@ControllerAdvice
public class ExceptionsHandler {

  @ExceptionHandler(RecordNotFoundException.class)
  ResponseEntity<Map<String, String>> recordNotFoundExceptionHandler(RecordNotFoundException e) {
    return new ResponseEntity<>(
        createExceptionResponse(e.getMessage(), HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  ResponseEntity<Map<String, String>> constraintViolationExceptionHandler(
      ConstraintViolationException e) {

    return new ResponseEntity<>(
        createExceptionResponse(e.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  ResponseEntity<Map<String, String>> methodArgumentNotValidExceptionHandler(
      MethodArgumentNotValidException e) {
    String failureMessage = getMethodArgumentNotValidExceptionMessage(e);

    return new ResponseEntity<>(
        createExceptionResponse(failureMessage, HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(CurrencyNotSupportedException.class)
  ResponseEntity<Map<String, String>> currencyNotSupportedExceptionHandler(
      CurrencyNotSupportedException e) {
    return new ResponseEntity<>(
        createExceptionResponse(e.getMessage(), HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
  }

  private Map<String, String> createExceptionResponse(String message, HttpStatus status) {
    Timestamp time = new Timestamp(System.currentTimeMillis());

    Map<String, String> response = new LinkedHashMap<>();
    response.put("timestamp", time.toString());
    response.put("code", status.toString());
    response.put("error", message);
    return response;
  }

  private String getMethodArgumentNotValidExceptionMessage(MethodArgumentNotValidException e) {
    String message = e.getDetailMessageArguments()[1].toString();
    return message.substring(1, message.length() - 1);
  }
}
