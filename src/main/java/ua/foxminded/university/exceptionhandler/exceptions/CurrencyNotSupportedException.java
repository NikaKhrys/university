/* (C)2023-2024 */
package ua.foxminded.university.exceptionhandler.exceptions;

/**
 * Runtime exception that occurs when user tries converting to unsupported currency
 *
 * @author veronika_khrystoforova
 */
public class CurrencyNotSupportedException extends RuntimeException {
  private static final String EXCEPTION_MESSAGE = "Can't convert value in currency %s";

  public CurrencyNotSupportedException(String currency) {
    super(EXCEPTION_MESSAGE.formatted(currency));
  }
}
