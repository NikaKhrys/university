/* (C)2023-2024 */
package ua.foxminded.university.exceptionhandler.exceptions;

/**
 * Runtime exception that occurs when user with given id/login wasn't found
 *
 * @author veronika_khrystoforova
 */
public class RecordNotFoundException extends RuntimeException {
  private static final String EXCEPTION_MESSAGE = "Record %s wasn't found";

  public RecordNotFoundException(Integer id) {
    super(EXCEPTION_MESSAGE.formatted(id));
  }

  public RecordNotFoundException(String login) {
    super(EXCEPTION_MESSAGE.formatted(login));
  }
}
