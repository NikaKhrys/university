/* (C)2023 */
package ua.foxminded.university.logging;

import java.util.HashMap;
import java.util.Map;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
  private static final String SEND_REQUEST_LOG =
      "Sending request to {} database: {} with parameters: {}";
  private static final String EXECUTION_FAILED_LOG =
      "Failed to execute request {} with exception message: {}";
  private static final String RESPONSE_LOG = "Get response for {} request with return value: {}";

  @Value("${dbName}")
  private String dbName;

  @Around("@annotation(log)")
  public Object logMethod(ProceedingJoinPoint joinPoint, Log log) throws Throwable {
    Logger logger = LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringType());
    String methodName = joinPoint.getSignature().getName();
    Map<String, Object> parameters = obtainParameters(joinPoint);
    logger.info(SEND_REQUEST_LOG, dbName, methodName, parameters);
    Object proceed;
    try {
      proceed = joinPoint.proceed();
    } catch (Throwable e) {
      logger.error(EXECUTION_FAILED_LOG, methodName, e.getMessage());
      throw e;
    }
    logger.info(RESPONSE_LOG, methodName, proceed);
    return proceed;
  }

  private Map<String, Object> obtainParameters(ProceedingJoinPoint joinPoint) {
    Map<String, Object> parameters = new HashMap<>();
    String[] parameterNames = ((CodeSignature) joinPoint.getSignature()).getParameterNames();
    Object[] parameterValues = joinPoint.getArgs();
    for (int i = 0; i < parameterNames.length && i < parameterValues.length; i++) {
      parameters.put(parameterNames[i], parameterValues[i]);
    }
    return parameters;
  }
}
