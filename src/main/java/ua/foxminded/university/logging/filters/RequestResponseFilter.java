package ua.foxminded.university.logging.filters;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ua.foxminded.university.logging.filters.requestcaching.CachedBodyHttpServletRequest;

@Component
public class RequestResponseFilter implements Filter {

  private static final Logger LOG = LoggerFactory.getLogger(RequestResponseFilter.class);

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) servletRequest;
    HttpServletResponse res = (HttpServletResponse) servletResponse;
    CachedBodyHttpServletRequest cachedBodyHttpServletRequest =
        new CachedBodyHttpServletRequest(req);
    LOG.info(
        "Logging Request {} : {} {}",
        req.getMethod(),
        req.getRequestURI(),
        getRequestBody(cachedBodyHttpServletRequest));
    filterChain.doFilter(cachedBodyHttpServletRequest, servletResponse);
    LOG.info(
        "Logging Response : status is {}, contentType is {}",
        res.getStatus(),
        res.getContentType());
  }

  private String getRequestBody(ServletRequest req) throws IOException {
    BufferedReader reader = req.getReader();
    String body = reader.lines().collect(Collectors.joining());
    if (!body.isBlank()) {
      body = "with body: " + body;
    }
    reader.close();
    return body;
  }
}
