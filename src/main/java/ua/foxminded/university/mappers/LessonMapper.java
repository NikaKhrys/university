/* (C)2024 */
package ua.foxminded.university.mappers;

import java.util.List;
import org.springframework.stereotype.Component;
import ua.foxminded.university.repository.entity.Auditorium;
import ua.foxminded.university.repository.entity.Course;
import ua.foxminded.university.repository.entity.Group;
import ua.foxminded.university.repository.entity.Lesson;
import ua.foxminded.university.repository.entity.Professor;
import ua.foxminded.university.service.dto.AuditoriumDto;
import ua.foxminded.university.service.dto.CourseDto;
import ua.foxminded.university.service.dto.GroupDto;
import ua.foxminded.university.service.dto.LessonDto;
import ua.foxminded.university.service.dto.ProfessorDto;

/**
 * Mapper for Lesson entity/dto
 *
 * @author veronika_khrystoforova
 */
@Component
public class LessonMapper {

  public LessonDto toDto(Lesson lesson) {
    Auditorium auditorium = lesson.getAuditorium();
    Course course = lesson.getCourse();
    Professor professor = lesson.getProfessor();
    List<Group> groups = lesson.getGroups();

    AuditoriumDto auditoriumDto = AuditoriumDto.builder().number(auditorium.getNumber()).build();
    CourseDto courseDto = CourseDto.builder().name(course.getName()).build();
    ProfessorDto professorDto =
        ProfessorDto.builder()
            .firstName(professor.getFirstName())
            .lastName(professor.getLastName())
            .build();
    List<GroupDto> groupDtos = mapToDto(groups);

    return LessonDto.builder()
        .date(lesson.getDate())
        .time(lesson.getTime())
        .auditorium(auditoriumDto)
        .course(courseDto)
        .professor(professorDto)
        .groups(groupDtos)
        .build();
  }

  private List<GroupDto> mapToDto(List<Group> groups) {
    return groups.stream().map(group -> GroupDto.builder().name(group.getName()).build()).toList();
  }
}
