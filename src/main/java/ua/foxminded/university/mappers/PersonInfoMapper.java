/* (C)2024 */
package ua.foxminded.university.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.springframework.beans.factory.annotation.Autowired;
import ua.foxminded.university.repository.entity.PersonInfo;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.util.AuditManager;

/**
 * Mapper for PersonInfo entity/dto
 *
 * @author veronika_khrystoforova
 */
@Mapper(componentModel = ComponentModel.SPRING)
public abstract class PersonInfoMapper {
  @Autowired AuditManager auditManager;

  @Mapping(target = "person", ignore = true)
  @Mapping(target = "audit", expression = "java(auditManager.createNewAudit(username))")
  public abstract PersonInfo toEntity(PersonInfoDto personInfoDto, String username);

  @Mapping(target = "person", ignore = true)
  public abstract PersonInfoDto toDto(PersonInfo personInfo);
}
