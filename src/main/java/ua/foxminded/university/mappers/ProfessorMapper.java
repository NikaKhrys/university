/* (C)2024 */
package ua.foxminded.university.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import ua.foxminded.university.repository.entity.Audit;
import ua.foxminded.university.repository.entity.Course;
import ua.foxminded.university.repository.entity.Professor;
import ua.foxminded.university.service.dto.ProfessorDto;
import ua.foxminded.university.util.AuditManager;

/**
 * Mapper for Professor entity/dto
 *
 * @author veronika_khrystoforova
 */
@Mapper(componentModel = ComponentModel.SPRING, imports = Course.class)
public abstract class ProfessorMapper {
  @Autowired PersonInfoMapper personInfoMapper;
  @Autowired AuditManager auditManager;

  @Mapping(
      target = "courses",
      expression =
          "java(professorDto.getCoursesId().stream().map(id ->"
              + " Course.builder().id(id).build()).toList())")
  @Mapping(
      target = "info",
      expression = "java(personInfoMapper.toEntity(professorDto.getInfo(), username))")
  @Mapping(target = "audit", ignore = true)
  @Mapping(target = "lessons", ignore = true)
  protected abstract Professor toEntityWithoutAudit(ProfessorDto professorDto, String username);

  public Professor toEntity(ProfessorDto professorDto, String username) {
    Professor professor = toEntityWithoutAudit(professorDto, username);
    Audit audit = auditManager.createNewAudit(username);
    professor.setAudit(audit);
    return professor;
  }

  @Mapping(
      target = "coursesId",
      expression = "java(professor.getCourses().stream().map(Course::getId).toList())")
  @Mapping(target = "info", expression = "java(personInfoMapper.toDto(professor.getInfo()))")
  @Mapping(target = "lessonsId", ignore = true)
  public abstract ProfessorDto toDto(Professor professor);

  public Professor toEntity(Professor professor, ProfessorDto professorDto, String username) {
    Professor updatedProfessor = toEntityWithoutAudit(professorDto, username);
    Audit audit = auditManager.updateAudit(professor.getAudit(), username);
    updatedProfessor.setAudit(audit);
    updatedProfessor.setId(professor.getId());
    return updatedProfessor;
  }

  public Page<ProfessorDto> toDtos(Page<Professor> professors) {
    return professors.map(this::toDto);
  }
}
