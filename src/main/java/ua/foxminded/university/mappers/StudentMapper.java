/* (C)2024 */
package ua.foxminded.university.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import ua.foxminded.university.repository.entity.Audit;
import ua.foxminded.university.repository.entity.Student;
import ua.foxminded.university.service.dto.StudentDto;
import ua.foxminded.university.util.AuditManager;

/**
 * Mapper for Student entity/dto
 *
 * @author veronika_khrystoforova
 */
@Mapper(componentModel = ComponentModel.SPRING)
public abstract class StudentMapper {
  @Autowired PersonInfoMapper personInfoMapper;
  @Autowired AuditManager auditManager;

  @Mapping(
      target = "faculty",
      expression = "java(Faculty.builder().id(studentDto.getFacultyId()).build())")
  @Mapping(
      target = "group",
      expression = "java(Group.builder().id(studentDto.getGroupId()).build())")
  @Mapping(
      target = "info",
      expression = "java(personInfoMapper.toEntity(studentDto.getInfo(), username))")
  @Mapping(target = "audit", ignore = true)
  protected abstract Student toEntityWithoutAudit(StudentDto studentDto, String username);

  public Student toEntity(StudentDto studentDto, String username) {
    Student student = toEntityWithoutAudit(studentDto, username);
    Audit audit = auditManager.createNewAudit(username);
    student.setAudit(audit);
    return student;
  }

  @Mapping(target = "facultyId", source = "student.faculty.id")
  @Mapping(target = "groupId", source = "student.group.id")
  @Mapping(target = "info", expression = "java(personInfoMapper.toDto(student.getInfo()))")
  public abstract StudentDto toDto(Student student);

  public Student toEntity(Student student, StudentDto studentDto, String username) {
    Student updatedStudent = toEntityWithoutAudit(studentDto, username);
    Audit audit = auditManager.updateAudit(student.getAudit(), username);
    updatedStudent.setAudit(audit);
    updatedStudent.setId(student.getId());
    return updatedStudent;
  }

  public Page<StudentDto> toDtos(Page<Student> students) {
    return students.map(this::toDto);
  }
}
