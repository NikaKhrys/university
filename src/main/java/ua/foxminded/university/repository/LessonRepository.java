/* (C)2023-2024 */
package ua.foxminded.university.repository;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.foxminded.university.repository.entity.Lesson;

/**
 * Repository class for the Lesson entity
 *
 * @author veronika_khrystoforova
 */
@Repository
@Transactional(readOnly = true)
public interface LessonRepository extends PagingAndSortingRepository<Lesson, Integer> {
  @Query(
      value =
          """
            SELECT l.*
            FROM students AS s
            JOIN groups AS g
            ON s.group_id = g.group_id
            JOIN lessons_groups AS lg
            ON lg.group_id=g.group_id
            JOIN lessons AS l
            ON l.lesson_id = lg.lesson_id
            WHERE s.id = :id AND l.lesson_date >= :startDate
            AND l.lesson_date <= :endDate""",
      nativeQuery = true)
  Page<Lesson> getStudentLessons(
      @Param("id") Integer studentId,
      @Param("startDate") LocalDate startDate,
      @Param("endDate") LocalDate endDate,
      Pageable pageable);

  @Query(
      value =
          """
            SELECT l.*
            FROM professors AS p
            JOIN lessons AS l
            ON p.id = l.professor_id
            WHERE p.id = :id AND l.lesson_date >= :startDate
            AND l.lesson_date <= :endDate
            """,
      nativeQuery = true)
  Page<Lesson> getProfessorLessons(
      @Param("id") Integer professorId,
      @Param("startDate") LocalDate startDate,
      @Param("endDate") LocalDate endDate,
      Pageable pageable);
}
