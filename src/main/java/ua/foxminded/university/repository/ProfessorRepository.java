/* (C)2023-2024 */
package ua.foxminded.university.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ua.foxminded.university.repository.entity.Professor;

/**
 * Repository class for the Professor entity
 *
 * @author veronika_khrystoforova
 */
@Repository
public interface ProfessorRepository
    extends PagingAndSortingRepository<Professor, Integer>, CrudRepository<Professor, Integer> {}
