/* (C)2023-2024 */
package ua.foxminded.university.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ua.foxminded.university.repository.entity.Student;

/**
 * Repository class for the Student entity
 *
 * @author veronika_khrystoforova
 */
@Repository
public interface StudentRepository
    extends PagingAndSortingRepository<Student, Integer>, CrudRepository<Student, Integer> {}
