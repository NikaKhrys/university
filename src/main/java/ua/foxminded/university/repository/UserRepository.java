/* (C)2023-2024 */
package ua.foxminded.university.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.foxminded.university.repository.entity.User;

/**
 * Repository class for the User entity
 *
 * @author veronika_khrystoforova
 */
@Transactional(readOnly = true)
@Repository
public interface UserRepository
    extends PagingAndSortingRepository<User, Integer>, CrudRepository<User, Integer> {
  Optional<User> findByLogin(String login);

  @Query("SELECT u.id FROM User u WHERE u.login = ?1")
  Optional<Integer> getIdByLogin(String login);
}
