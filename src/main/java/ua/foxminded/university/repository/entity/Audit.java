/* (C)2024 */
package ua.foxminded.university.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * An embeddable Audit entity with service columns
 *
 * @author veronika_khrystoforova
 */
@Embeddable
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Audit {

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "created_datetime")
  private Instant createdDatetime;

  @Column(name = "updated_by")
  private String updatedBy;

  @Column(name = "updated_datetime")
  private Instant updatedDatetime;
}
