/* (C)2023-2024 */
package ua.foxminded.university.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "auditoriums")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = "lessons")
@ToString(exclude = "lessons")
public class Auditorium {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "auditorium_id")
  private Integer id;

  @Column(name = "auditorium_number")
  private Integer number;

  @OneToMany(mappedBy = "auditorium")
  private List<Lesson> lessons;

  @Embedded private Audit audit;
}
