/* (C)2023-2024 */
package ua.foxminded.university.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "faculties")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"students", "groups"})
@ToString(exclude = {"students", "groups"})
public class Faculty {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "faculty_id")
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  @OneToMany(mappedBy = "faculty")
  private List<Group> groups;

  @OneToMany(mappedBy = "faculty")
  private List<Student> students;

  @Embedded private Audit audit;
}
