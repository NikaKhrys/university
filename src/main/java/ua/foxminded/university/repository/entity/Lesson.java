/* (C)2023-2024 */
package ua.foxminded.university.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "lessons")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = "groups")
@ToString(exclude = "groups")
public class Lesson {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "lesson_id")
  private Integer id;

  @Column(name = "lesson_date")
  private LocalDate date;

  @Column(name = "lesson_time")
  private LocalTime time;

  @ManyToOne
  @JoinColumn(name = "auditorium_id")
  private Auditorium auditorium;

  @ManyToOne
  @JoinColumn(name = "course_id")
  private Course course;

  @ManyToOne
  @JoinColumn(name = "professor_id", referencedColumnName = "id")
  private Professor professor;

  @ManyToMany
  @JoinTable(
      name = "lessons_groups",
      joinColumns = {@JoinColumn(name = "lesson_id")},
      inverseJoinColumns = {@JoinColumn(name = "group_id")})
  private List<Group> groups;

  @Embedded private Audit audit;
}
