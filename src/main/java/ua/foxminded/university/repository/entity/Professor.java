/* (C)2023-2024 */
package ua.foxminded.university.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "professors")
@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"courses", "lessons"})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Professor extends Person<Professor> {
  @ManyToMany
  @JoinTable(
      name = "professors_courses",
      joinColumns = {@JoinColumn(name = "professor_id")},
      inverseJoinColumns = {@JoinColumn(name = "course_id")})
  private List<Course> courses;

  @OneToMany(mappedBy = "professor")
  private List<Lesson> lessons;

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
