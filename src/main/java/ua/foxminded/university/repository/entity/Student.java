/* (C)2023-2024 */
package ua.foxminded.university.repository.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "students")
@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"faculty", "group"})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Student extends Person<Student> {
  @ManyToOne
  @JoinColumn(name = "faculty_id")
  private Faculty faculty;

  @Column(name = "student_year")
  private Integer year;

  @ManyToOne
  @JoinColumn(name = "group_id")
  private Group group;

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
