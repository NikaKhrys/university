/* (C)2023-2024 */
package ua.foxminded.university.security;

import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import ua.foxminded.university.service.impls.UserDetailsServiceImpl;

/**
 * The class is used by the spring method security configuration to define if the current user is
 * allowed to get access for an entry point.
 *
 * @author veronika_khrystoforova
 */
@Component
public class SecurityChecker {
  private final UserDetailsServiceImpl userDetailsService;

  @Autowired
  public SecurityChecker(UserDetailsServiceImpl userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  public boolean checkIfRecordIsStudents(Authentication authentication, @PathVariable Integer id) {
    List<String> authorities =
        authentication.getAuthorities().stream().map(Objects::toString).toList();
    if (authorities.contains("PROFESSOR")) return false;
    return id.equals(userDetailsService.getIdByLogin(authentication.getName()));
  }

  public boolean checkIfRecordIsProfessors(
      Authentication authentication, @PathVariable Integer id) {
    List<String> authorities =
        authentication.getAuthorities().stream().map(Objects::toString).toList();
    if (authorities.contains("STUDENT")) return false;
    return id.equals(userDetailsService.getIdByLogin(authentication.getName()));
  }

  public boolean checkIfTransactionIsPersons(
      Authentication authentication, @PathVariable Integer id) {
    return id.equals(userDetailsService.getIdByLogin(authentication.getName()));
  }
}
