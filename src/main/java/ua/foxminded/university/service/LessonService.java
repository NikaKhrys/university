/* (C)2023-2024 */
package ua.foxminded.university.service;

import java.time.LocalDate;
import java.time.YearMonth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.service.dto.LessonDto;

/**
 * Service class for Lesson
 *
 * @author veronika_khrystoforova
 */
public interface LessonService {

  /**
   * Gets student schedule for given date
   *
   * @param studentId - id of the student whose schedule is requested
   * @param date - date for which schedule is requested
   * @param pageable - number and size of the page with lessons
   * @return page with lessons for given date
   */
  Page<LessonDto> getStudentLessonsForDate(Integer studentId, LocalDate date, Pageable pageable);

  /**
   * Gets student schedule for given month
   *
   * @param studentId - id of the student whose schedule is requested
   * @param month - month for which schedule is requested
   * @param pageable - number and size of the page with lessons
   * @return page with lessons for given month
   */
  Page<LessonDto> getStudentLessonsForMonth(Integer studentId, YearMonth month, Pageable pageable);

  /**
   * Gets professor schedule for given date
   *
   * @param professorId - id of the professor whose schedule is requested
   * @param date - date for which schedule is requested
   * @param pageable - number and size of the page with lessons
   * @return page with lessons for given date
   */
  Page<LessonDto> getProfessorLessonsForDate(
      Integer professorId, LocalDate date, Pageable pageable);

  /**
   * Gets professor schedule for given month
   *
   * @param professorId - id of the professor whose schedule is requested
   * @param month - month for which schedule is requested
   * @param pageable - number and size of the page with lessons
   * @return page with lessons for given month
   */
  Page<LessonDto> getProfessorLessonsForMonth(
      Integer professorId, YearMonth month, Pageable pageable);
}
