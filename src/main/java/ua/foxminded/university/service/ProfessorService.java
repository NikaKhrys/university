/* (C)2023-2024 */
package ua.foxminded.university.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.service.dto.ProfessorDto;

/**
 * Service class for Professor
 *
 * @author veronika_khrystoforova
 */
public interface ProfessorService {
  /**
   * Finds all professors records
   *
   * @param pageable - number and size of the returning page
   * @return Page<ProfessorDto> - found professors if any
   */
  Page<ProfessorDto> findAll(Pageable pageable);

  /**
   * Finds professor by given id if such professor exists
   *
   * @param id - id of the searched professor
   * @return ProfessorDto - found professor if any
   */
  ProfessorDto findById(Integer id);

  /**
   * Saves new professor record to the db
   *
   * @param professorDto - professor information to be saved
   * @return ProfessorDto - saved professor with id
   */
  ProfessorDto save(ProfessorDto professorDto, String username);

  /**
   * Updates professor's record by id if such record exists
   *
   * @param id - id of the professor to be updated
   * @param professorDto - new professor information
   * @return ProfessorDto - updated professor
   */
  ProfessorDto update(Integer id, ProfessorDto professorDto, String username);

  /**
   * Deletes professor record by id from the db if such record exists
   *
   * @param id - id of the professor to be deleted
   */
  void deleteById(Integer id);
}
