/* (C)2023-2024 */
package ua.foxminded.university.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.service.dto.StudentDto;

/**
 * Service class for Student
 *
 * @author veronika_khrystoforova
 */
public interface StudentService {

  /**
   * Finds all students records
   *
   * @param pageable - number and size of the returning page
   * @return Page<StudentDto> - found students if any
   */
  Page<StudentDto> findAll(Pageable pageable);

  /**
   * Finds student by given id if such student exists
   *
   * @param id - id of the searched student
   * @return StudentDto - found student if any
   */
  StudentDto findById(Integer id);

  /**
   * Saves new student record to the db
   *
   * @param studentDto - student information to be saved
   * @param username - name of the user who attempts to make a save to the db
   * @return StudentDto - saved student with id
   */
  StudentDto save(StudentDto studentDto, String username);

  /**
   * Updates student's record by id if such record exists
   *
   * @param id - id of the student to be updated
   * @param studentDto - new student information
   * @param username - name of the user who attempts to make a save to the db
   * @return StudentDto - updated student
   */
  StudentDto update(Integer id, StudentDto studentDto, String username);

  /**
   * Deletes student record by id from the db if such record exists
   *
   * @param id - id of the student to be deleted
   */
  void deleteById(Integer id);
}
