/* (C)2023-2024 */
package ua.foxminded.university.service;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.service.dto.TransactionDto;

/**
 * Service class for Transaction
 *
 * @author veronika_khrystoforova
 */
public interface TransactionService {

  /**
   * Gets money transactions for user by their id in given currency in given time range
   *
   * @param userId - id of the user whose transactions are requested
   * @param startDate - date from which transactions are requested
   * @param endDate - date to which transactions are requested
   * @param currency - currency in which transactions are requested
   * @param pageable - number and size of the page with transactions
   * @return page with transactions information
   */
  Page<TransactionDto> getTransactionsInCurrency(
      Integer userId, LocalDate startDate, LocalDate endDate, String currency, Pageable pageable);
}
