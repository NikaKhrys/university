/* (C)2023-2024 */
package ua.foxminded.university.service.constraint;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator class that checks if phone number matches the required pattern
 *
 * @author veronika_khrystoforova
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

  private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("380\\d{9}");

  @Override
  public void initialize(PhoneNumber constraintAnnotation) {}

  @Override
  public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
    if (value == null) {
      return false;
    }
    Matcher matcher = PHONE_NUMBER_PATTERN.matcher(value);
    return matcher.matches();
  }
}
