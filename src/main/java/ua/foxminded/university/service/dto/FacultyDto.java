/* (C)2023-2024 */
package ua.foxminded.university.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"groupsId", "studentsId"})
@ToString(exclude = {"groupsId", "studentsId"})
public class FacultyDto {
  private Integer id;
  private String name;
  private String description;
  private List<Integer> groupsId;
  private List<Integer> studentsId;
}
