/* (C)2023-2024 */
package ua.foxminded.university.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LessonDto {
  private Integer id;
  private LocalDate date;
  private LocalTime time;
  private AuditoriumDto auditorium;
  private CourseDto course;
  private ProfessorDto professor;
  private List<GroupDto> groups;
}
