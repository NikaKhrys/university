/* (C)2023-2024 */
package ua.foxminded.university.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.foxminded.university.service.constraint.PhoneNumber;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonInfoDto {
  private Integer id;

  @PhoneNumber private String phoneNumber;

  @NotBlank @Email private String email;

  @NotBlank
  @Size(min = 5, max = 70)
  private String address;

  private PersonDto<?> person;
}
