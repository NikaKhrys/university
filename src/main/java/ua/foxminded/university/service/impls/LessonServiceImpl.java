/* (C)2023-2024 */
package ua.foxminded.university.service.impls;

import java.time.LocalDate;
import java.time.YearMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.foxminded.university.logging.Log;
import ua.foxminded.university.mappers.LessonMapper;
import ua.foxminded.university.repository.LessonRepository;
import ua.foxminded.university.repository.entity.Lesson;
import ua.foxminded.university.service.LessonService;
import ua.foxminded.university.service.dto.LessonDto;

@Service
public class LessonServiceImpl implements LessonService {
  private final LessonRepository repository;
  private final LessonMapper mapper;

  @Autowired
  public LessonServiceImpl(LessonRepository repository, LessonMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  @Log
  public Page<LessonDto> getStudentLessonsForDate(
      Integer studentId, LocalDate date, Pageable pageable) {
    Page<Lesson> lessons = repository.getStudentLessons(studentId, date, date, pageable);
    return lessons.map(mapper::toDto);
  }

  @Log
  public Page<LessonDto> getStudentLessonsForMonth(
      Integer studentId, YearMonth month, Pageable pageable) {
    LocalDate startDate = month.atDay(1);
    LocalDate endDate = month.atEndOfMonth();

    Page<Lesson> lessons = repository.getStudentLessons(studentId, startDate, endDate, pageable);
    return lessons.map(mapper::toDto);
  }

  @Log
  public Page<LessonDto> getProfessorLessonsForDate(
      Integer professorId, LocalDate date, Pageable pageable) {
    Page<Lesson> lessons = repository.getProfessorLessons(professorId, date, date, pageable);

    return lessons.map(mapper::toDto);
  }

  @Log
  public Page<LessonDto> getProfessorLessonsForMonth(
      Integer professorId, YearMonth month, Pageable pageable) {
    LocalDate startDate = month.atDay(1);
    LocalDate endDate = month.atEndOfMonth();

    Page<Lesson> lessons =
        repository.getProfessorLessons(professorId, startDate, endDate, pageable);
    return lessons.map(mapper::toDto);
  }
}
