/* (C)2023-2024 */
package ua.foxminded.university.service.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.logging.Log;
import ua.foxminded.university.mappers.ProfessorMapper;
import ua.foxminded.university.repository.ProfessorRepository;
import ua.foxminded.university.repository.entity.Professor;
import ua.foxminded.university.service.ProfessorService;
import ua.foxminded.university.service.dto.ProfessorDto;

@Service
public class ProfessorServiceImpl implements ProfessorService {
  private final ProfessorRepository repository;
  private final ProfessorMapper mapper;

  @Autowired
  public ProfessorServiceImpl(ProfessorRepository repository, ProfessorMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  @Log
  public Page<ProfessorDto> findAll(Pageable pageable) {
    Page<Professor> professors = repository.findAll(pageable);
    return mapper.toDtos(professors);
  }

  @Log
  public ProfessorDto findById(Integer id) {
    Professor professor =
        repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));

    return mapper.toDto(professor);
  }

  @Log
  public ProfessorDto save(ProfessorDto professorDto, String username) {
    Professor professor = mapper.toEntity(professorDto, username);
    Professor savedProfessor = repository.save(professor);
    return mapper.toDto(savedProfessor);
  }

  @Log
  @Transactional
  public ProfessorDto update(Integer id, ProfessorDto professorDto, String username) {
    Professor professor =
        repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));

    Professor professorToBeUpdated = mapper.toEntity(professor, professorDto, username);
    Professor updatedProfessor = repository.save(professorToBeUpdated);
    return mapper.toDto(updatedProfessor);
  }

  @Log
  @Transactional
  public void deleteById(Integer id) {
    Professor professor =
        repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));
    repository.delete(professor);
  }
}
