/* (C)2023-2024 */
package ua.foxminded.university.service.impls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.logging.Log;
import ua.foxminded.university.mappers.StudentMapper;
import ua.foxminded.university.repository.StudentRepository;
import ua.foxminded.university.repository.entity.Student;
import ua.foxminded.university.service.StudentService;
import ua.foxminded.university.service.dto.StudentDto;

@Service
public class StudentServiceImpl implements StudentService {

  private final StudentRepository repository;
  private final StudentMapper mapper;

  @Autowired
  public StudentServiceImpl(StudentRepository repository, StudentMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  @Log
  public Page<StudentDto> findAll(Pageable pageable) {
    Page<Student> students = repository.findAll(pageable);
    return mapper.toDtos(students);
  }

  @Log
  public StudentDto findById(Integer id) {
    Student student = repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));

    return mapper.toDto(student);
  }

  @Log
  public StudentDto save(StudentDto studentDto, String username) {
    Student student = mapper.toEntity(studentDto, username);
    Student savedStudent = repository.save(student);
    return mapper.toDto(savedStudent);
  }

  @Log
  @Transactional
  public StudentDto update(Integer id, StudentDto studentDto, String username) {
    Student student = repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));
    Student studentToBeUpdated = mapper.toEntity(student, studentDto, username);
    Student updatedStudent = repository.save(studentToBeUpdated);
    return mapper.toDto(updatedStudent);
  }

  @Log
  @Transactional
  public void deleteById(Integer id) {
    Student student = repository.findById(id).orElseThrow(() -> new RecordNotFoundException(id));
    repository.delete(student);
  }
}
