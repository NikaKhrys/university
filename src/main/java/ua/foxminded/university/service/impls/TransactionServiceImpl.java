/* (C)2023-2024 */
package ua.foxminded.university.service.impls;

import java.time.LocalDate;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;
import ua.foxminded.university.exceptionhandler.exceptions.CurrencyNotSupportedException;
import ua.foxminded.university.service.TransactionService;
import ua.foxminded.university.service.dto.TransactionDto;

@Service
public class TransactionServiceImpl implements TransactionService {
  @Value("${transactionsUrl}")
  private String baseUrl;

  public Page<TransactionDto> getTransactionsInCurrency(
      Integer userId, LocalDate startDate, LocalDate endDate, String currency, Pageable pageable) {

    ResponseSpec response = getResponse(userId, startDate, endDate, currency, pageable);
    PageWrapper<TransactionDto> pageWrapper =
        response
            .bodyToMono(new ParameterizedTypeReference<PageWrapper<TransactionDto>>() {})
            .block();

    if (pageWrapper == null) {
      return Page.empty();
    }

    List<TransactionDto> transactions = pageWrapper.getContent();
    verifyCurrencyConversion(transactions);

    return new PageImpl<>(transactions, pageable, pageWrapper.getTotalElements());
  }

  private ResponseSpec getResponse(
      Integer personId,
      LocalDate startDate,
      LocalDate endDate,
      String currency,
      Pageable pageable) {
    return WebClient.builder()
        .baseUrl(baseUrl)
        .build()
        .get()
        .uri(
            uriBuilder ->
                uriBuilder
                    .path(personId.toString())
                    .queryParam("page", pageable.getPageNumber())
                    .queryParam("size", pageable.getPageSize())
                    .queryParam("startDate", startDate)
                    .queryParam("endDate", endDate)
                    .queryParam("currency", currency)
                    .build())
        .retrieve();
  }

  private void verifyCurrencyConversion(List<TransactionDto> transactions) {
    boolean currencyIsIncorrect =
        transactions.stream().anyMatch(transactionDto -> transactionDto.getValue() == -1);
    if (currencyIsIncorrect) {
      throw new CurrencyNotSupportedException(transactions.get(0).getCurrency());
    }
  }

  @Getter
  private static class PageWrapper<T> {
    private List<T> content;
    private long totalElements;
  }
}
