/* (C)2023-2024 */
package ua.foxminded.university.service.impls;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.repository.UserRepository;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository userRepository;

  @Autowired
  public UserDetailsServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    return userRepository
        .findByLogin(login)
        .map(
            user ->
                new User(
                    user.getLogin(), user.getPassword(), Collections.singleton(user.getRole())))
        .orElseThrow(() -> new RecordNotFoundException(login));
  }

  public Integer getIdByLogin(String login) {
    return userRepository.getIdByLogin(login).orElseThrow(() -> new RecordNotFoundException(login));
  }
}
