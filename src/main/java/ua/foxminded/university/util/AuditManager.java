/* (C)2024 */
package ua.foxminded.university.util;

import java.time.Instant;
import java.util.Objects;
import org.springframework.stereotype.Component;
import ua.foxminded.university.repository.entity.Audit;

/**
 * Class that manages filling of the service columns
 *
 * @author veronika_khrystoforova
 */
@Component
public class AuditManager {
  String DEFAULT_USERNAME = "system";
  Instant DEFAULT_TIME = Instant.now();

  public Audit createNewAudit(String username) {
    String createdBy = Objects.isNull(username) ? DEFAULT_USERNAME : username;
    return Audit.builder()
        .createdBy(createdBy)
        .createdDatetime(DEFAULT_TIME)
        .updatedBy(createdBy)
        .updatedDatetime(DEFAULT_TIME)
        .build();
  }

  public Audit updateAudit(Audit oldAudit, String username) {
    String updatedBy = Objects.isNull(username) ? DEFAULT_USERNAME : username;
    oldAudit.setUpdatedBy(updatedBy);
    oldAudit.setUpdatedDatetime(DEFAULT_TIME);
    return oldAudit;
  }
}
