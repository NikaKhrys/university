INSERT INTO auditoriums (auditorium_number) VALUES (101);
INSERT INTO auditoriums (auditorium_number) VALUES (102);
INSERT INTO auditoriums (auditorium_number) VALUES (103);

INSERT INTO faculties (name,description) VALUES ('Business','Business description');

INSERT INTO groups (name,faculty_id) VALUES ('B1',1);
INSERT INTO groups (name,faculty_id) VALUES ('B2',1);

INSERT INTO persons_info (phone_number, email,address) VALUES ('380111111111','email1@gmail.com','address1');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380222222222','email2@gmail.com','address2');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380333333333','email3@gmail.com','address3');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380444444444','email4@gmail.com','address4');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380555555555','email5@gmail.com','address5');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380666666666','email6@gmail.com','address6');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380777777777','email7@gmail.com','address7');
INSERT INTO persons_info (phone_number, email,address) VALUES ('380888888888','email8@gmail.com','address8');

INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Milly','Smith',1,1,1,1);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Sam','Smith',1,1,1,2);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Tom','Lee',1,1,1,3);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Marta','Rodrigues',1,2,2,4);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Jack','Hill',1,2,2,5);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Stephen','Smith',1,2,2,6);

INSERT INTO professors (first_name,last_name,person_info_id) VALUES ('Bob','Martin',7);
INSERT INTO professors (first_name,last_name,person_info_id) VALUES ('Sam','Lee',8);

INSERT INTO courses (name,description) VALUES ('Math','Math description');
INSERT INTO courses (name,description) VALUES ('Logistics','Logistics description');
INSERT INTO courses (name,description) VALUES ('Literature','Literature description');

INSERT INTO professors_courses (professor_id,course_id) VALUES (7,1);
INSERT INTO professors_courses (professor_id,course_id) VALUES (7,2);
INSERT INTO professors_courses (professor_id,course_id) VALUES (7,3);
INSERT INTO professors_courses (professor_id,course_id) VALUES (8,1);
INSERT INTO professors_courses (professor_id,course_id) VALUES (8,3);

INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-01','08:30:00',1,1,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-04','10:30:00',2,3,8);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-04','12:30:00',3,2,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-07','08:30:00',2,2,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-10','10:30:00',1,1,8);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-10','10:30:00',3,1,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-12','12:30:00',2,3,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-12','12:30:00',3,3,8);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-12','13:30:00',1,1,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-16','08:30:00',2,1,8);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-16','10:30:00',2,1,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-20','08:30:00',3,1,8);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-21','08:30:00',1,2,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-24','10:30:00',2,3,7);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-24','10:30:00',1,3,8);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-24','12:30:00',1,1,7);

INSERT INTO lessons_groups (lesson_id,group_id) VALUES (1,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (2,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (4,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (5,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (7,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (9,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (10,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (11,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (13,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (15,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (1,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (2,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (3,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (6,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (8,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (9,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (10,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (12,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (14,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (16,2);

INSERT INTO users (person_id,login,password,role) VALUES(7,'bobmartin','$2a$10$STsUV1gWULfZjXFw6r7UZeTSTwTc9zA0nRhsHcjooRbjEA7zDeSWW','ADMIN');
INSERT INTO users (person_id,login,password,role) VALUES(1,'millysmith','$2a$10$fBaLOUkszMbCpc5vRcBYQuCBrRWPvyy957r1vXE6IpmTftQmOAKR2','STUDENT');
INSERT INTO users (person_id,login,password,role) VALUES(8,'samlee','$2a$10$smaizNpAQKZ3uooVuhkZCuaA/35uZsFWvMaedkAe5v/4ec6LxV/aK','PROFESSOR');