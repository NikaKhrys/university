DROP TABLE IF EXISTS auditoriums, students, professors, courses, persons_info, groups, faculties, professors_courses;

CREATE SEQUENCE IF NOT EXISTS person_seq
    INCREMENT BY 1
    START WITH 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
create table auditoriums(
	auditorium_id serial primary key,
	auditorium_number int
);
create table faculties(
	faculty_id serial primary key,
	name varchar(30),
	description varchar(100)
);
create table groups(
	group_id serial primary key,
	name varchar(20),
	faculty_id int,
	CONSTRAINT faculties_groups_id_fk
		FOREIGN KEY (faculty_id)
		REFERENCES faculties (faculty_id)
		ON UPDATE CASCADE
);
create table persons_info(
	person_info_id serial primary key,
	phone_number varchar(20),
	email varchar(40),
	address varchar(90)
);

create table students(
	id int DEFAULT NEXTVAL('person_seq') primary key,
	first_name varchar(30),
	last_name varchar(30),
	faculty_id int,
	student_year int,
	group_id int,
	person_info_id int,
	CONSTRAINT persons_info_students_fk_id
		FOREIGN KEY (person_info_id)
		REFERENCES persons_info (person_info_id)
		ON UPDATE CASCADE,
	CONSTRAINT faculties_students_id_fk
		FOREIGN KEY (faculty_id)
		REFERENCES faculties (faculty_id)
		ON UPDATE CASCADE,
	CONSTRAINT groups_students_id_fk
		FOREIGN KEY (group_id)
		REFERENCES groups (group_id)
		ON UPDATE CASCADE
);
create table professors(
	id int DEFAULT NEXTVAL('person_seq') primary key,
	first_name varchar(30),
	last_name varchar(30),
	person_info_id int,
	CONSTRAINT persons_info_professors_fk_id
		FOREIGN KEY (person_info_id)
		REFERENCES persons_info (person_info_id)
		ON UPDATE CASCADE
);
create table courses(
	course_id serial primary key,
	name varchar(50),
	description varchar(100)
);
create table lessons(
	lesson_id serial primary key,
	lesson_date date,
	lesson_time time(0),
	auditorium_id int,
	course_id int,
	professor_id int,
	CONSTRAINT auditoriums_lessons_fk_id
		FOREIGN KEY (auditorium_id)
		REFERENCES auditoriums (auditorium_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT courses_lessons_fk_id
		FOREIGN KEY (course_id)
		REFERENCES courses (course_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT professors_lessons_fk_id
		FOREIGN KEY (professor_id)
		REFERENCES professors (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

create table lessons_groups(
	lesson_id int,
	group_id int,
	PRIMARY KEY (lesson_id, group_id),
	CONSTRAINT lessons_groups_fk_id
		FOREIGN KEY (lesson_id)
		REFERENCES lessons (lesson_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT groups_lessons_fk_id
		FOREIGN KEY (group_id)
		REFERENCES groups (group_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

create table professors_courses(
	professor_id int,
	course_id int,
	PRIMARY KEY (professor_id,course_id),
	CONSTRAINT professors_courses_id_fk
		FOREIGN KEY (professor_id)
		REFERENCES professors (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT courses_professors_id_fk
		FOREIGN KEY (course_id)
		REFERENCES courses (course_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);



