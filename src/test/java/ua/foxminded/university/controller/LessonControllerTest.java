/* (C)2023-2024 */
package ua.foxminded.university.controller;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ua.foxminded.university.config.SecurityConfiguration;
import ua.foxminded.university.controller.impls.LessonControllerImpl;
import ua.foxminded.university.security.SecurityChecker;
import ua.foxminded.university.service.LessonService;
import ua.foxminded.university.service.dto.AuditoriumDto;
import ua.foxminded.university.service.dto.CourseDto;
import ua.foxminded.university.service.dto.GroupDto;
import ua.foxminded.university.service.dto.LessonDto;
import ua.foxminded.university.service.dto.ProfessorDto;
import ua.foxminded.university.service.impls.UserDetailsServiceImpl;

@WebMvcTest(value = LessonControllerImpl.class)
@Import({SecurityConfiguration.class})
class LessonControllerTest {
  @Autowired private MockMvc mvc;
  @MockBean private LessonService service;

  @MockBean(name = "securityChecker")
  private SecurityChecker checker;

  @MockBean private UserDetailsServiceImpl userDetailsService;

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsAdmin()
      throws Exception {
    LocalDate date = LocalDate.of(2023, 1, 10);
    Pageable pageable = PageRequest.of(0, 1);
    List<LessonDto> lessons = getTestData();

    when(service.getStudentLessonsForDate(1, date, pageable))
        .thenReturn(new PageImpl<>(lessons.subList(0, 1), pageable, 2));

    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2023-01-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content[0].date", is("2023-01-10")))
        .andExpect(jsonPath("$.content[0].time", is("10:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(2)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("group1")))
        .andExpect(jsonPath("$.content[0].groups[1].name", is("group2")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("firstName")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("course")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));

    verify(service, times(1)).getStudentLessonsForDate(1, date, pageable);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void
      getStudentSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsStudentWithAccess()
          throws Exception {
    LocalDate date = LocalDate.of(2023, 1, 10);
    Pageable pageable = PageRequest.of(0, 1);
    List<LessonDto> lessons = getTestData();

    when(checker.checkIfRecordIsStudents(any(Authentication.class), any(Integer.class)))
        .thenReturn(true);
    when(service.getStudentLessonsForDate(1, date, pageable))
        .thenReturn(new PageImpl<>(lessons.subList(0, 1), pageable, 2));

    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2023-01-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content[0].date", is("2023-01-10")))
        .andExpect(jsonPath("$.content[0].time", is("10:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(2)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("group1")))
        .andExpect(jsonPath("$.content[0].groups[1].name", is("group2")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("firstName")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("course")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));

    verify(service, times(1)).getStudentLessonsForDate(1, date, pageable);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void getStudentSchedule_shouldReturnUnauthorized_whenDateIsNotNullAndUserIsStudentWithoutAccess()
      throws Exception {

    when(userDetailsService.getIdByLogin(any())).thenReturn(2);
    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2023-01-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void getStudentSchedule_shouldReturnUnauthorized_whenDateIsNotNullAndUserIsProfessor()
      throws Exception {

    when(userDetailsService.getIdByLogin(any())).thenReturn(1);
    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2023-01-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnNoContent_whenDateIsNotNullAndPageIsEmpty() throws Exception {
    LocalDate date = LocalDate.of(2023, 5, 7);
    Pageable pageable = PageRequest.of(0, 5);
    when(service.getStudentLessonsForDate(1, date, pageable))
        .thenReturn(new PageImpl<>(new LinkedList<>()));

    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2023-05-07")
                .param("page", "0")
                .param("size", "5"))
        .andExpect(status().isNoContent());
    verify(service, times(1)).getStudentLessonsForDate(1, date, pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnLessonsForMonth_whenMonthIsNotNull() throws Exception {
    Pageable pageable = PageRequest.of(0, 3);
    YearMonth month = YearMonth.now().withMonth(2);
    List<LessonDto> lessons = getTestData().subList(2, 3);

    when(service.getStudentLessonsForMonth(1, month, pageable))
        .thenReturn(new PageImpl<>(lessons, pageable, 1));

    mvc.perform(get("/lessons/student/1").param("month", "2").param("page", "0").param("size", "3"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].date", is("2023-02-01")))
        .andExpect(jsonPath("$.content[0].time", is("08:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(1)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("group1")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("firstName")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("course")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(1)))
        .andExpect(jsonPath("$.totalElements", is(1)));
    verify(service, times(1)).getStudentLessonsForMonth(1, month, pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnNoContent_whenMonthIsNotNullAndPageIsEmpty()
      throws Exception {
    YearMonth month = YearMonth.now().withMonth(5);
    Pageable pageable = PageRequest.of(0, 2);

    when(service.getStudentLessonsForMonth(1, month, pageable))
        .thenReturn(new PageImpl<>(new LinkedList<>()));

    mvc.perform(get("/lessons/student/1").param("month", "5").param("page", "0").param("size", "2"))
        .andExpect(status().isNoContent());
    verify(service, times(1)).getStudentLessonsForMonth(1, month, pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNotNull() throws Exception {
    mvc.perform(
            get("/lessons/student/1")
                .param("month", "1")
                .param("date", "2023-01-01")
                .param("page", "1")
                .param("size", "1"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNull() throws Exception {
    mvc.perform(get("/lessons/student/1").param("page", "1").param("size", "1"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_ReturnBadRequest_WhenMonthIsNotPositive() throws Exception {
    mvc.perform(get("/lessons/student/1").param("page", "1").param("size", "1").param("month", "0"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthIsGreaterThan12() throws Exception {
    mvc.perform(
            get("/lessons/student/1").param("page", "1").param("size", "1").param("month", "20"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsAdmin()
      throws Exception {
    LocalDate date = LocalDate.of(2023, 2, 1);
    Pageable pageable = PageRequest.of(0, 1);
    List<LessonDto> lessons = getTestData();

    when(service.getProfessorLessonsForDate(1, date, pageable))
        .thenReturn(new PageImpl<>(lessons.subList(2, 3), pageable, 1));

    mvc.perform(
            get("/lessons/professor/1")
                .param("date", "2023-02-01")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].date", is("2023-02-01")))
        .andExpect(jsonPath("$.content[0].time", is("08:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(1)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("group1")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("firstName")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("course")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(1)))
        .andExpect(jsonPath("$.totalElements", is(1)));
    verify(service, times(1)).getProfessorLessonsForDate(1, date, pageable);
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void getProfessorSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsProfessor()
      throws Exception {
    LocalDate date = LocalDate.of(2023, 2, 1);
    Pageable pageable = PageRequest.of(0, 1);
    List<LessonDto> lessons = getTestData();

    when(service.getProfessorLessonsForDate(1, date, pageable))
        .thenReturn(new PageImpl<>(lessons.subList(2, 3), pageable, 1));

    mvc.perform(
            get("/lessons/professor/1")
                .param("date", "2023-02-01")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].date", is("2023-02-01")))
        .andExpect(jsonPath("$.content[0].time", is("08:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(1)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("group1")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("firstName")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("course")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(1)))
        .andExpect(jsonPath("$.totalElements", is(1)));
    verify(service, times(1)).getProfessorLessonsForDate(1, date, pageable);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void getProfessorSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsStudent()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/1")
                .param("date", "2023-02-01")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnNoContent_whenDateIsNotNullAndPageIsEmpty()
      throws Exception {
    LocalDate date = LocalDate.of(2023, 1, 1);
    Pageable pageable = PageRequest.of(0, 2);

    when(service.getProfessorLessonsForDate(1, date, pageable))
        .thenReturn(new PageImpl<>(new LinkedList<>()));

    mvc.perform(
            get("/lessons/professor/1")
                .param("date", "2023-01-01")
                .param("page", "0")
                .param("size", "2"))
        .andExpect(status().isNoContent());
    verify(service, times(1)).getProfessorLessonsForDate(1, date, pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnLessonPageForMonth_whenMonthIsNotNull() throws Exception {
    YearMonth month = YearMonth.now().withMonth(1);
    Pageable pageable = PageRequest.of(0, 1);
    List<LessonDto> lessons = getTestData();

    when(service.getProfessorLessonsForMonth(1, month, pageable))
        .thenReturn(new PageImpl<>(lessons.subList(0, 2), pageable, 2));

    mvc.perform(
            get("/lessons/professor/1").param("month", "1").param("page", "0").param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content[0].date", is("2023-01-10")))
        .andExpect(jsonPath("$.content[0].time", is("10:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(2)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("group1")))
        .andExpect(jsonPath("$.content[0].groups[1].name", is("group2")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("firstName")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("course")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));

    verify(service, times(1)).getProfessorLessonsForMonth(1, month, pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnNoContent_whenMonthIsNotNullAndPageIsEmpty()
      throws Exception {
    YearMonth month = YearMonth.now().withMonth(5);
    Pageable pageable = PageRequest.of(0, 1);

    when(service.getProfessorLessonsForMonth(1, month, pageable))
        .thenReturn(new PageImpl<>(new LinkedList<>()));

    mvc.perform(
            get("/lessons/professor/1").param("month", "5").param("page", "0").param("size", "1"))
        .andExpect(status().isNoContent());

    verify(service, times(1)).getProfessorLessonsForMonth(1, month, pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNotNull()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/2")
                .param("month", "2")
                .param("date", "2023-02-01")
                .param("page", "1")
                .param("size", "2"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNull() throws Exception {
    mvc.perform(get("/lessons/professor/1").param("page", "1").param("size", "1"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthIsNotPositive() throws Exception {
    mvc.perform(
            get("/lessons/professor/1").param("page", "1").param("size", "1").param("month", "0"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthIsGreaterThan12() throws Exception {
    mvc.perform(
            get("/lessons/professor/1").param("page", "1").param("size", "1").param("month", "20"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  private List<LessonDto> getTestData() {
    AuditoriumDto auditorium = AuditoriumDto.builder().number(101).build();
    CourseDto course = CourseDto.builder().name("course").build();
    ProfessorDto professor =
        ProfessorDto.builder().firstName("firstName").lastName("lastName").build();
    GroupDto group1 = GroupDto.builder().name("group1").build();
    GroupDto group2 = GroupDto.builder().name("group2").build();

    LessonDto lesson1 =
        LessonDto.builder()
            .date(LocalDate.of(2023, 1, 10))
            .time(LocalTime.of(10, 30))
            .auditorium(auditorium)
            .course(course)
            .professor(professor)
            .groups(List.of(group1, group2))
            .build();

    LessonDto lesson2 =
        LessonDto.builder()
            .date(LocalDate.of(2023, 1, 10))
            .time(LocalTime.of(12, 20))
            .auditorium(auditorium)
            .course(course)
            .professor(professor)
            .groups(List.of(group2))
            .build();

    LessonDto lesson3 =
        LessonDto.builder()
            .date(LocalDate.of(2023, 2, 1))
            .time(LocalTime.of(8, 30))
            .auditorium(auditorium)
            .course(course)
            .professor(professor)
            .groups(List.of(group1))
            .build();
    return List.of(lesson1, lesson2, lesson3);
  }
}
