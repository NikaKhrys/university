/* (C)2023-2024 */
package ua.foxminded.university.controller;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ua.foxminded.university.config.SecurityConfiguration;
import ua.foxminded.university.controller.impls.ProfessorControllerImpl;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.security.SecurityChecker;
import ua.foxminded.university.service.ProfessorService;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.ProfessorDto;
import ua.foxminded.university.service.impls.UserDetailsServiceImpl;

@WebMvcTest(value = ProfessorControllerImpl.class)
@Import({SecurityConfiguration.class})
class ProfessorControllerTest {
  @Autowired private MockMvc mvc;
  @MockBean private ProfessorService service;

  @MockBean(name = "securityChecker")
  private SecurityChecker checker;

  @MockBean private UserDetailsServiceImpl userDetailsService;

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnProfessorsPage_whenPageIsNotEmptyAndUserIsAdmin() throws Exception {
    Pageable pageable = PageRequest.of(0, 2);
    List<ProfessorDto> professors = getTestData();

    when(service.findAll(pageable))
        .thenReturn(new PageImpl<>(professors, pageable, professors.size()));

    mvc.perform(get("/professors").param("page", "0").param("size", "2"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].firstName", is("firstName1")))
        .andExpect(jsonPath("$.content[1].firstName", is("firstName2")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(1)))
        .andExpect(jsonPath("$.totalElements", is(2)));
    verify(service, times(1)).findAll(pageable);
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void findAll_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    mvc.perform(get("/professors").param("page", "0").param("size", "2"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnNoContent_whenPageIsEmpty() throws Exception {
    Pageable pageable = PageRequest.of(1, 2);

    when(service.findAll(pageable)).thenReturn(new PageImpl<>(new LinkedList<>()));

    mvc.perform(get("/professors").param("page", "1").param("size", "2"))
        .andExpect(status().isNoContent());

    verify(service, times(1)).findAll(pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnProfessor_whenProfessorExistsAndUserIsAdmin() throws Exception {
    ProfessorDto professor = getTestData().get(0);
    when(service.findById(1)).thenReturn(professor);

    mvc.perform(get("/professors/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("firstName1")))
        .andExpect(jsonPath("$.lastName", is("lastName1")))
        .andExpect(jsonPath("$.coursesId", hasSize(2)))
        .andExpect(jsonPath("$.coursesId[0]", is(1)))
        .andExpect(jsonPath("$.coursesId[1]", is(2)))
        .andExpect(jsonPath("$.info.email", is("email1@gmail.com")));
    verify(service, times(1)).findById(1);
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void findById_shouldReturnProfessor_whenProfessorExistsAndUserIsProfessorWithAccess()
      throws Exception {
    ProfessorDto professor = getTestData().get(0);

    when(service.findById(1)).thenReturn(professor);
    when(checker.checkIfRecordIsProfessors(any(Authentication.class), any(Integer.class)))
        .thenReturn(true);

    mvc.perform(get("/professors/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("firstName1")))
        .andExpect(jsonPath("$.lastName", is("lastName1")))
        .andExpect(jsonPath("$.coursesId", hasSize(2)))
        .andExpect(jsonPath("$.coursesId[0]", is(1)))
        .andExpect(jsonPath("$.coursesId[1]", is(2)))
        .andExpect(jsonPath("$.info.email", is("email1@gmail.com")));
    verify(service, times(1)).findById(1);
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void findById_shouldReturnUnauthorized_whenUserIsProfessorWithoutAccess() throws Exception {

    mvc.perform(get("/professors/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void findById_shouldReturnUnauthorized_whenUserIsStudent() throws Exception {
    mvc.perform(get("/professors/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnErrorMessage_whenProfessorDoesNotExist() throws Exception {
    when(service.findById(4)).thenThrow(new RecordNotFoundException(4));

    mvc.perform(get("/professors/4"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", is(anything())))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 4 wasn't found")));

    verify(service, times(1)).findById(4);
  }

  @Test
  @WithMockUser(authorities = "ADMIN", username = "name")
  void save_shouldReturnProfessorWithId_whenProfessorIsValidAndUserIsAdmin() throws Exception {
    ProfessorDto professor = getTestData().get(1);
    ProfessorDto professorWithId = getTestData().get(1);
    professorWithId.setId(2);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    when(service.save(professor, "name")).thenReturn(professorWithId);

    mvc.perform(post("/professors").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.firstName", is("firstName2")));
    verify(service, times(1)).save(professor, "name");
  }

  @Test
  void save_shouldReturnProfessorWithId_whenUserIsUnauthorized() throws Exception {
    ProfessorDto professor = getTestData().get(1);
    ProfessorDto professorWithId = getTestData().get(1);
    professorWithId.setId(2);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    when(service.save(professor, null)).thenReturn(professorWithId);

    mvc.perform(post("/professors").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.firstName", is("firstName2")));
    verify(service, times(1)).save(professor, null);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void save_shouldReturnBadRequest_whenProfessorIsNotValid() throws Exception {
    ProfessorDto professor = getTestData().get(0);
    professor.setCoursesId(null);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(post("/professors").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN", username = "name")
  void update_shouldReturnUpdatedProfessor_whenProfessorExistsAndUserIsAdmin() throws Exception {
    ProfessorDto professor = getTestData().get(0);
    ProfessorDto professorWithId = getTestData().get(0);
    professorWithId.setId(2);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    when(service.update(2, professor, "name")).thenReturn(professorWithId);

    mvc.perform(put("/professors/2").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.firstName", is("firstName1")));
    verify(service, times(1)).update(2, professor, "name");
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR", username = "name")
  void update_shouldReturnUpdatedProfessor_whenProfessorExistsAndUserIsProfessorWithAccess()
      throws Exception {
    ProfessorDto professor = getTestData().get(0);
    ProfessorDto professorWithId = getTestData().get(0);
    professorWithId.setId(2);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    when(service.update(2, professor, "name")).thenReturn(professorWithId);
    when(checker.checkIfRecordIsProfessors(any(Authentication.class), any(Integer.class)))
        .thenReturn(true);

    mvc.perform(put("/professors/2").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.firstName", is("firstName1")));
    verify(service, times(1)).update(2, professor, "name");
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void update_shouldReturnUnauthorized_whenUserIsProfessorWithoutAccess() throws Exception {
    ProfessorDto professor = getTestData().get(0);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/2").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void update_shouldReturnUnauthorized_whenUserIsStudent() throws Exception {
    ProfessorDto professor = getTestData().get(0);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/2").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN", username = "name")
  void update_shouldReturnErrorMessage_whenProfessorDoesNotExist() throws Exception {
    ProfessorDto professor = getTestData().get(1);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    when(service.update(4, professor, "name")).thenThrow(new RecordNotFoundException(4));

    mvc.perform(put("/professors/4").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 4 wasn't found")));
    verify(service, times(1)).update(4, professor, "name");
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnBadRequest_whenProfessorIsNotValid() throws Exception {
    ProfessorDto professor = getTestData().get(0);
    professor.setFirstName("     ");
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/1").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnNoContent_whenProfessorExistsAndUserIsAdmin() throws Exception {
    mvc.perform(delete("/professors/1")).andExpect(status().isNoContent());
    verify(service, times(1)).deleteById(1);
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void deleteById_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    mvc.perform(delete("/professors/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnErrorMessage_whenProfessorDoesNotExist() throws Exception {
    doThrow(new RecordNotFoundException(5)).when(service).deleteById(5);

    mvc.perform(delete("/professors/5"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 5 wasn't found")));
    verify(service, times(1)).deleteById(5);
  }

  private List<ProfessorDto> getTestData() {
    PersonInfoDto personInfo1 =
        PersonInfoDto.builder()
            .email("email1@gmail.com")
            .address("address1")
            .phoneNumber("380111111111")
            .build();
    PersonInfoDto personInfo2 =
        PersonInfoDto.builder()
            .email("email2@gmail.com")
            .address("address2")
            .phoneNumber("380222222222")
            .build();

    ProfessorDto professor1 =
        ProfessorDto.builder()
            .coursesId(List.of(1, 2))
            .firstName("firstName1")
            .lastName("lastName1")
            .info(personInfo1)
            .build();
    ProfessorDto professor2 =
        ProfessorDto.builder()
            .coursesId(List.of(2))
            .firstName("firstName2")
            .lastName("lastName2")
            .info(personInfo2)
            .build();
    return List.of(professor1, professor2);
  }
}
