/* (C)2023-2024 */
package ua.foxminded.university.controller;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ua.foxminded.university.config.SecurityConfiguration;
import ua.foxminded.university.controller.impls.StudentControllerImpl;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.security.SecurityChecker;
import ua.foxminded.university.service.StudentService;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.StudentDto;
import ua.foxminded.university.service.impls.UserDetailsServiceImpl;

@WebMvcTest(value = StudentControllerImpl.class)
@Import({SecurityConfiguration.class})
class StudentControllerTest {

  @Autowired private MockMvc mvc;
  @MockBean private StudentService service;

  @MockBean(name = "securityChecker")
  private SecurityChecker checker;

  @MockBean private UserDetailsServiceImpl userDetailsService;

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnStudentsPage_whenPageIsNotEmptyAndUserIsAdmin() throws Exception {
    Pageable pageable = PageRequest.of(1, 1);
    List<StudentDto> students = getTestData();

    when(service.findAll(pageable))
        .thenReturn(new PageImpl<>(students.subList(1, 2), pageable, students.size()));

    mvc.perform(get("/students").param("page", "1").param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].firstName", is("firstName2")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
    verify(service, times(1)).findAll(pageable);
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void findAll_shouldReturnStudentsPage_whenPageIsNotEmptyAndUserIsProfessor() throws Exception {
    Pageable pageable = PageRequest.of(1, 1);
    List<StudentDto> students = getTestData();

    when(service.findAll(pageable))
        .thenReturn(new PageImpl<>(students.subList(1, 2), pageable, students.size()));

    mvc.perform(get("/students").param("page", "1").param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].firstName", is("firstName2")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
    verify(service, times(1)).findAll(pageable);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void findAll_shouldReturnUnauthorized_whenUserIsStudent() throws Exception {
    mvc.perform(get("/students").param("page", "1").param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnNoContent_whenPageIsEmpty() throws Exception {
    Pageable pageable = PageRequest.of(4, 5);

    when(service.findAll(pageable)).thenReturn(new PageImpl<>(new LinkedList<>()));

    mvc.perform(get("/students").param("page", "4").param("size", "5"))
        .andExpect(status().isNoContent());
    verify(service, times(1)).findAll(pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnStudent_whenStudentExistsAndUserIsAdmin() throws Exception {
    StudentDto student = getTestData().get(0);
    when(service.findById(1)).thenReturn(student);

    mvc.perform(get("/students/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("firstName1")))
        .andExpect(jsonPath("$.lastName", is("lastName1")))
        .andExpect(jsonPath("$.year", is(1)))
        .andExpect(jsonPath("$.groupId", is(1)))
        .andExpect(jsonPath("$.facultyId", is(1)))
        .andExpect(jsonPath("$.info.email", is("email1@gmail.com")));
    verify(service, times(1)).findById(1);
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void findById_shouldReturnStudent_whenStudentExistsUserIsProfessor() throws Exception {
    StudentDto student = getTestData().get(0);
    when(service.findById(1)).thenReturn(student);

    mvc.perform(get("/students/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("firstName1")))
        .andExpect(jsonPath("$.lastName", is("lastName1")))
        .andExpect(jsonPath("$.year", is(1)))
        .andExpect(jsonPath("$.groupId", is(1)))
        .andExpect(jsonPath("$.facultyId", is(1)))
        .andExpect(jsonPath("$.info.email", is("email1@gmail.com")));
    verify(service, times(1)).findById(1);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void findById_shouldReturnStudent_whenStudentExistsAndUserIsStudentWithAccess() throws Exception {
    StudentDto student = getTestData().get(0);

    when(service.findById(1)).thenReturn(student);
    when(checker.checkIfRecordIsStudents(any(Authentication.class), any(Integer.class)))
        .thenReturn(true);

    mvc.perform(get("/students/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("firstName1")))
        .andExpect(jsonPath("$.lastName", is("lastName1")))
        .andExpect(jsonPath("$.year", is(1)))
        .andExpect(jsonPath("$.groupId", is(1)))
        .andExpect(jsonPath("$.facultyId", is(1)))
        .andExpect(jsonPath("$.info.email", is("email1@gmail.com")));
    verify(service, times(1)).findById(1);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void findById_shouldReturnUnauthorized_whenStudentExistsAndUserIsStudentWithoutAccess()
      throws Exception {
    mvc.perform(get("/students/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnErrorMessage_whenStudentDoesNotExist() throws Exception {
    when(service.findById(5)).thenThrow(new RecordNotFoundException(5));

    mvc.perform(get("/students/5"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 5 wasn't found")));
    verify(service, times(1)).findById(5);
  }

  @Test
  @WithMockUser(authorities = "ADMIN", username = "name")
  void save_shouldReturnStudentWithId_whenStudentIsValidAndUserIsAdmin() throws Exception {
    StudentDto student = getTestData().get(0);
    StudentDto studentWithId = getTestData().get(0);
    studentWithId.setId(3);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    when(service.save(student, "name")).thenReturn(studentWithId);

    mvc.perform(post("/students").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(3)))
        .andExpect(jsonPath("$.firstName", is("firstName1")));
    verify(service, times(1)).save(student, "name");
  }

  @Test
  void save_shouldReturnUnauthorized_whenUserIsUnauthorized() throws Exception {
    StudentDto student = getTestData().get(0);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(post("/students").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void save_shouldReturnBadRequest_whenStudentIsNotValid() throws Exception {
    StudentDto student = getTestData().get(0);
    student.setYear(8);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(post("/students").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN", username = "name")
  void update_shouldReturnUpdatedStudent_whenStudentExistsAndUserIsAdmin() throws Exception {
    StudentDto student = getTestData().get(1);
    StudentDto studentWithId = getTestData().get(1);
    studentWithId.setId(2);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    when(service.update(2, student, "name")).thenReturn(studentWithId);

    mvc.perform(put("/students/2").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.lastName", is("lastName2")));
    verify(service, times(1)).update(2, student, "name");
  }

  @Test
  @WithMockUser(authorities = "STUDENT", username = "name")
  void update_shouldReturnUpdatedStudent_whenStudentExistsAndUserIsStudentWithAccess()
      throws Exception {
    StudentDto student = getTestData().get(1);
    StudentDto studentWithId = getTestData().get(1);
    studentWithId.setId(2);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    when(service.update(2, student, "name")).thenReturn(studentWithId);
    when(checker.checkIfRecordIsStudents(any(Authentication.class), any(Integer.class)))
        .thenReturn(true);

    mvc.perform(put("/students/2").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.lastName", is("lastName2")));
    verify(service, times(1)).update(2, student, "name");
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void update_shouldReturnUnauthorized_whenStudentExistsAndUserIsStudentWithoutAccess()
      throws Exception {
    StudentDto student = getTestData().get(1);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/2").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void update_shouldReturnUnauthorized_whenStudentExistsAndUserIsProfessor() throws Exception {
    StudentDto student = getTestData().get(1);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/2").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN", username = "name")
  void update_shouldReturnErrorMessage_whenStudentDoesNotExist() throws Exception {
    StudentDto student = getTestData().get(0);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    when(service.update(1, student, "name")).thenThrow(new RecordNotFoundException(1));

    mvc.perform(put("/students/1").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 1 wasn't found")));
    verify(service, times(1)).update(1, student, "name");
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnBadRequest_whenStudentIsNotValid() throws Exception {
    StudentDto student = getTestData().get(0);
    student.setInfo(null);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/1").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", is(anything())));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnNoContent_whenStudentExistsAndUserIsAdmin() throws Exception {
    mvc.perform(delete("/students/1")).andExpect(status().isNoContent());
    verify(service, times(1)).deleteById(1);
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void deleteById_shouldReturnNoContent_whenUserIsNotAdmin() throws Exception {
    mvc.perform(delete("/students/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnErrorMessage_whenStudentDoesNotExist() throws Exception {
    doThrow(new RecordNotFoundException(5)).when(service).deleteById(5);

    mvc.perform(delete("/students/5"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 5 wasn't found")));
    verify(service, times(1)).deleteById(5);
  }

  private List<StudentDto> getTestData() {
    PersonInfoDto personInfo1 =
        PersonInfoDto.builder()
            .email("email1@gmail.com")
            .address("address1")
            .phoneNumber("380111111111")
            .build();
    PersonInfoDto personInfo2 =
        PersonInfoDto.builder()
            .email("email2@gmail.com")
            .address("address2")
            .phoneNumber("380222222222")
            .build();
    StudentDto student1 =
        StudentDto.builder()
            .year(1)
            .groupId(1)
            .facultyId(1)
            .firstName("firstName1")
            .lastName("lastName1")
            .info(personInfo1)
            .build();
    StudentDto student2 =
        StudentDto.builder()
            .year(2)
            .groupId(1)
            .facultyId(1)
            .firstName("firstName2")
            .lastName("lastName2")
            .info(personInfo2)
            .build();
    return List.of(student1, student2);
  }
}
