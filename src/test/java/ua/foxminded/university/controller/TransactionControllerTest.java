/* (C)2023-2024 */
package ua.foxminded.university.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ua.foxminded.university.config.SecurityConfiguration;
import ua.foxminded.university.controller.impls.TransactionControllerImpl;
import ua.foxminded.university.exceptionhandler.exceptions.CurrencyNotSupportedException;
import ua.foxminded.university.security.SecurityChecker;
import ua.foxminded.university.service.TransactionService;
import ua.foxminded.university.service.dto.TransactionDto;
import ua.foxminded.university.service.impls.UserDetailsServiceImpl;

@WebMvcTest(value = TransactionControllerImpl.class)
@Import({SecurityConfiguration.class})
class TransactionControllerTest {
  @Autowired private MockMvc mvc;
  @MockBean private TransactionService service;

  @MockBean(name = "securityChecker")
  SecurityChecker checker;

  @MockBean private UserDetailsServiceImpl userDetailsService;

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getTransactionsInCurrency_shouldReturnTransactionsPage_whenPageIsNotEmptyAndUserIsAdmin()
      throws Exception {
    Pageable pageable = PageRequest.of(0, 2);
    List<TransactionDto> transactions = getTestData();
    LocalDate startDate = LocalDate.of(2023, 1, 1);
    LocalDate endDate = LocalDate.of(2023, 2, 1);

    when(service.getTransactionsInCurrency(1, startDate, endDate, "USD", pageable))
        .thenReturn(new PageImpl<>(transactions, pageable, 3));

    mvc.perform(
            get("/transactions/1")
                .param("page", "0")
                .param("size", "2")
                .param("startDate", "2023-01-01")
                .param("endDate", "2023-02-01")
                .param("currency", "USD"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].id", is(1)))
        .andExpect(jsonPath("$.content[0].currency", is("USD")))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(3)));

    verify(service, times(1)).getTransactionsInCurrency(1, startDate, endDate, "USD", pageable);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void getTransactionsInCurrency_shouldReturnTransactionsPage_whenPageIsNotEmptyAndUserWithAccess()
      throws Exception {
    Pageable pageable = PageRequest.of(0, 2);
    List<TransactionDto> transactions = getTestData();
    LocalDate startDate = LocalDate.of(2023, 1, 1);
    LocalDate endDate = LocalDate.of(2023, 2, 1);

    when(service.getTransactionsInCurrency(1, startDate, endDate, "USD", pageable))
        .thenReturn(new PageImpl<>(transactions, pageable, 3));
    when(checker.checkIfTransactionIsPersons(any(Authentication.class), any(Integer.class)))
        .thenReturn(true);

    mvc.perform(
            get("/transactions/1")
                .param("page", "0")
                .param("size", "2")
                .param("startDate", "2023-01-01")
                .param("endDate", "2023-02-01")
                .param("currency", "USD"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].id", is(1)))
        .andExpect(jsonPath("$.content[0].currency", is("USD")))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(3)));

    verify(service, times(1)).getTransactionsInCurrency(1, startDate, endDate, "USD", pageable);
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void getTransactionsInCurrency_shouldReturnUnauthorized_whenUserWithoutAccess() throws Exception {
    when(userDetailsService.getIdByLogin(any())).thenReturn(2);

    mvc.perform(
            get("/transactions/1")
                .param("page", "0")
                .param("size", "2")
                .param("startDate", "2023-01-01")
                .param("endDate", "2023-02-01")
                .param("currency", "USD"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  void getTransactionsInCurrency_shouldReturnUnauthorized_whenUserUnauthorized() throws Exception {
    mvc.perform(
            get("/transactions/1")
                .param("page", "0")
                .param("size", "2")
                .param("startDate", "2023-01-01")
                .param("endDate", "2023-02-01")
                .param("currency", "USD"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getTransactionsInCurrency_shouldReturnNoContent_whenPageIsEmpty() throws Exception {
    Pageable pageable = PageRequest.of(0, 2);
    LocalDate startDate = LocalDate.of(2023, 1, 1);
    LocalDate endDate = LocalDate.of(2023, 2, 1);

    when(service.getTransactionsInCurrency(1, startDate, endDate, "USD", pageable))
        .thenReturn(Page.empty());

    mvc.perform(
            get("/transactions/1")
                .param("page", "0")
                .param("size", "2")
                .param("startDate", "2023-01-01")
                .param("endDate", "2023-02-01")
                .param("currency", "USD"))
        .andExpect(status().isNoContent());

    verify(service, times(1)).getTransactionsInCurrency(1, startDate, endDate, "USD", pageable);
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getTransactionsInCurrency_shouldReturnBadRequest_whenCurrencyDoesNotExist()
      throws Exception {
    Pageable pageable = PageRequest.of(0, 2);
    LocalDate startDate = LocalDate.of(2023, 1, 1);
    LocalDate endDate = LocalDate.of(2023, 2, 1);

    when(service.getTransactionsInCurrency(1, startDate, endDate, "ULL", pageable))
        .thenThrow(CurrencyNotSupportedException.class);

    mvc.perform(
            get("/transactions/1")
                .param("page", "0")
                .param("size", "2")
                .param("startDate", "2023-01-01")
                .param("endDate", "2023-02-01")
                .param("currency", "ULL"))
        .andExpect(status().isBadRequest());

    verify(service, times(1)).getTransactionsInCurrency(1, startDate, endDate, "ULL", pageable);
  }

  private List<TransactionDto> getTestData() {
    TransactionDto transaction1 =
        TransactionDto.builder()
            .id(1)
            .personId(1)
            .position("STUDENT")
            .currency("USD")
            .value(79)
            .date(LocalDate.of(2023, 1, 1))
            .purpose("SCHOLARSHIP")
            .build();
    TransactionDto transaction2 =
        TransactionDto.builder()
            .id(2)
            .personId(1)
            .position("STUDENT")
            .currency("USD")
            .value(79)
            .date(LocalDate.of(2023, 2, 1))
            .purpose("SCHOLARSHIP")
            .build();
    return List.of(transaction1, transaction2);
  }
}
