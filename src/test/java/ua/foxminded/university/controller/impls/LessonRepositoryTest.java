/* (C)2023-2024 */
package ua.foxminded.university.controller.impls;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import ua.foxminded.university.repository.LessonRepository;
import ua.foxminded.university.repository.entity.Lesson;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Sql(
    value = {"/lesson/populateTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(
    value = {"/lesson/cleanTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class LessonRepositoryTest {
  @Autowired LessonRepository repository;
  Pageable pageable = PageRequest.of(0, 10);

  @Test
  void getStudentLessons_ReturnLessonsPageForDay_WhenStartAndEndDateAreEqual() {
    LocalDate testDate = LocalDate.of(2024, 1, 4);
    Page<Lesson> lessons = repository.getStudentLessons(1, testDate, testDate, pageable);
    List<Lesson> lessonList = lessons.toList();

    assertThat(lessons.getTotalElements()).isEqualTo(2);
    assertThat(lessonList.get(0)).hasFieldOrPropertyWithValue("id", 2);
    assertThat(lessonList.get(1)).hasFieldOrPropertyWithValue("id", 3);
  }

  @Test
  void getStudentLessons_ReturnLessonsPageForRange_WhenStartAndEndDateAreNotEqual() {
    LocalDate testStartDate = LocalDate.of(2024, 1, 1);
    LocalDate testEndDate = LocalDate.of(2024, 1, 31);
    Page<Lesson> lessons = repository.getStudentLessons(1, testStartDate, testEndDate, pageable);
    List<Lesson> lessonList = lessons.toList();

    assertThat(lessons.getTotalElements()).isEqualTo(3);
    assertThat(lessonList.get(0)).hasFieldOrPropertyWithValue("id", 1);
    assertThat(lessonList.get(1)).hasFieldOrPropertyWithValue("id", 2);
    assertThat(lessonList.get(2)).hasFieldOrPropertyWithValue("id", 3);
  }

  @Test
  void getStudentLessons_ReturnEmptyPage_WhenNoLessons() {
    LocalDate testDate = LocalDate.of(2023, 1, 1);
    Page<Lesson> lessons = repository.getStudentLessons(2, testDate, testDate, pageable);

    assertThat(lessons.getTotalElements()).isEqualTo(0);
  }

  @Test
  void getStudentLesson_ReturnEmptyPage_WhenStudentDoesNotExist() {
    LocalDate testStartDate = LocalDate.of(2023, 1, 1);
    LocalDate testEndDate = LocalDate.of(2023, 1, 10);
    Page<Lesson> lessons = repository.getStudentLessons(3, testStartDate, testEndDate, pageable);

    assertThat(lessons.getTotalElements()).isEqualTo(0);
  }

  @Test
  void getProfessorLessons_ReturnLessonsPageForDay_WhenStartAndEndDateAreEqual() {
    LocalDate testDate = LocalDate.of(2024, 2, 10);
    Page<Lesson> lessons = repository.getProfessorLessons(4, testDate, testDate, pageable);
    List<Lesson> lessonList = lessons.toList();

    assertThat(lessons.getTotalElements()).isEqualTo(2);
    assertThat(lessonList.get(0)).hasFieldOrPropertyWithValue("id", 5);
    assertThat(lessonList.get(1)).hasFieldOrPropertyWithValue("id", 6);
  }

  @Test
  void getProfessorLessons_ReturnLessonsPageForRange_WhenStartAndEndDateAreNotEqual() {
    LocalDate testStartDate = LocalDate.of(2024, 2, 1);
    LocalDate testEndDate = LocalDate.of(2024, 2, 28);
    Page<Lesson> lessons = repository.getProfessorLessons(4, testStartDate, testEndDate, pageable);
    List<Lesson> lessonList = lessons.toList();

    assertThat(lessons.getTotalElements()).isEqualTo(2);
    assertThat(lessonList.get(0)).hasFieldOrPropertyWithValue("id", 5);
    assertThat(lessonList.get(1)).hasFieldOrPropertyWithValue("id", 6);
  }

  @Test
  void getProfessorLessonsForMonth_ReturnEmptyPage_WhenNoLessons() {
    LocalDate testStartDate = LocalDate.of(2023, 2, 1);
    LocalDate testEndDate = LocalDate.of(2023, 2, 28);
    Page<Lesson> lessons = repository.getProfessorLessons(3, testStartDate, testEndDate, pageable);

    assertThat(lessons.getTotalElements()).isEqualTo(0);
  }

  @Test
  void getProfessorLessons_ReturnEmptyPage_WhenProfessorDoesNotExist() {
    LocalDate testDate = LocalDate.of(2023, 1, 1);
    Page<Lesson> lessons = repository.getProfessorLessons(1, testDate, testDate, pageable);

    assertThat(lessons.getTotalElements()).isEqualTo(0);
  }
}
