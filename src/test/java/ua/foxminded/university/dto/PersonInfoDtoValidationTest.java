/* (C)2023-2024 */
package ua.foxminded.university.dto;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.Set;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import ua.foxminded.university.service.dto.PersonInfoDto;

class PersonInfoDtoValidationTest {
  private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

  private PersonInfoDto personInfoDto;

  @Test
  void noViolations_WhenAllConstraintsAreMet() {
    personInfoDto = getPersonInfoDtoWithoutViolations();

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(0));
  }

  @Test
  void returnsExceptionMessage_WhenPhoneNumberIsNull() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setPhoneNumber(null);

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("must be a valid phone number. found: ${validatedValue}"));
  }

  @Test
  void returnsExceptionMessage_WhenPhoneNumberIsInvalid() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setPhoneNumber("15");

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("must be a valid phone number. found: ${validatedValue}"));
  }

  @Test
  void returnsExceptionMessage_WhenEmailIsInvalid() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setEmail("invalidEmail");

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Email.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenEmailIsBlank() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setEmail("");

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotBlank.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenAddressIsBlank() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setAddress(null);

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotBlank.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenAddressIsSmallerThan5() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setAddress("Ad");

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenAddressIsLongerThan70() {
    personInfoDto = getPersonInfoDtoWithoutViolations();
    personInfoDto.setAddress(RandomStringUtils.randomAlphabetic(80));

    Set<ConstraintViolation<PersonInfoDto>> constraintViolationSet =
        validator.validate(personInfoDto);

    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  private PersonInfoDto getPersonInfoDtoWithoutViolations() {
    return PersonInfoDto.builder()
        .email("email@gmail.com")
        .address("address")
        .phoneNumber("380555555555")
        .build();
  }
}
