/* (C)2023-2024 */
package ua.foxminded.university.dto;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.ProfessorDto;

class ProfessorDtoValidationTest {
  private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
  private ProfessorDto professorDto;

  @Test
  void noViolations_WhenAllConstraintsAreMet() {
    professorDto = getProfessorDtoWithoutViolations();

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(0));
  }

  @Test
  void returnsExceptionMessage_WhenFirstNameIsBlank() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.setFirstName(null);

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotBlank.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenFirstNameIsLongerThan30() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.setFirstName(RandomStringUtils.randomAlphabetic(40));

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenLastNameIsBlank() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.setLastName("   ");

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotBlank.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenLastNameIsLongerThan30() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.setLastName(RandomStringUtils.randomAlphabetic(40));

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenCoursesIdIsNull() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.setCoursesId(null);

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotNull.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenPersonInfoIsNull() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.setInfo(null);

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotNull.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenPersonInfoHasViolations() {
    professorDto = getProfessorDtoWithoutViolations();
    professorDto.getInfo().setAddress("Ad");

    Set<ConstraintViolation<ProfessorDto>> constraintViolationSet =
        validator.validate(professorDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  private ProfessorDto getProfessorDtoWithoutViolations() {
    PersonInfoDto personInfo =
        PersonInfoDto.builder()
            .email("email@gmail.com")
            .address("address")
            .phoneNumber("380555555555")
            .build();
    return ProfessorDto.builder()
        .coursesId(List.of(1, 2))
        .firstName("Name")
        .lastName("LastName")
        .info(personInfo)
        .build();
  }
}
