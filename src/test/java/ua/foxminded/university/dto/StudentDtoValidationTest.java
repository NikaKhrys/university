/* (C)2023-2024 */
package ua.foxminded.university.dto;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import java.util.Set;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.StudentDto;

class StudentDtoValidationTest {
  private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
  private StudentDto studentDto;

  @Test
  void noViolations_WhenAllConstraintsAreMet() {
    studentDto = getStudentDtoWithoutViolations();

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(0));
  }

  @Test
  void returnsExceptionMessage_WhenFirstNameIsBlank() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setFirstName(null);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotBlank.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenFirstNameIsLongerThan30() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setFirstName(RandomStringUtils.randomAlphabetic(40));

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenLastNameIsBlank() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setLastName("");

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotBlank.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenLastNameIsGraterThan30() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setLastName(RandomStringUtils.randomAlphabetic(40));

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Size.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenFacultyIdIsNull() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setFacultyId(null);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotNull.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenYearIsNull() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setYear(null);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotNull.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenYearIsNegative() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setYear(-1);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Positive.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenYearIsGreaterThen4() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setYear(11);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Max.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenGroupIdIsNull() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setGroupId(null);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotNull.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenPersonInfoIsNull() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setInfo(null);

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.NotNull.message}"));
  }

  @Test
  void returnsExceptionMessage_WhenPersonInfoHasViolations() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.getInfo().setEmail("invalidEmail");

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(1));
    assertThat(
        constraintViolationSet.stream().toList().get(0).getMessageTemplate(),
        equalTo("{jakarta.validation.constraints.Email.message}"));
  }

  @Test
  void returnsExceptionsMessages_WhenMoreThanOneViolations() {
    studentDto = getStudentDtoWithoutViolations();
    studentDto.setGroupId(null);
    studentDto.getInfo().setEmail("invalidEmail");

    Set<ConstraintViolation<StudentDto>> constraintViolationSet = validator.validate(studentDto);
    assertThat(constraintViolationSet.size(), equalTo(2));
  }

  private StudentDto getStudentDtoWithoutViolations() {
    PersonInfoDto personInfo =
        PersonInfoDto.builder()
            .email("email@gmail.com")
            .address("address")
            .phoneNumber("380555555555")
            .build();
    return StudentDto.builder()
        .year(1)
        .facultyId(1)
        .groupId(1)
        .firstName("Name")
        .lastName("LastName")
        .info(personInfo)
        .build();
  }
}
