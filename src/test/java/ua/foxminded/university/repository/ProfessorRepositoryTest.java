/* (C)2023-2024 */
package ua.foxminded.university.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import ua.foxminded.university.repository.entity.Audit;
import ua.foxminded.university.repository.entity.PersonInfo;
import ua.foxminded.university.repository.entity.Professor;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Sql(
    value = {"/professor/populateTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(
    value = {"/professor/cleanTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class ProfessorRepositoryTest {

  @Autowired ProfessorRepository repository;
  Pageable pageable = PageRequest.of(0, 10);

  @Test
  void findById_shouldReturnProfessor_whenProfessorExist() {
    Professor professor = repository.findById(1).get();

    assertThat(professor).hasFieldOrPropertyWithValue("id", 1);
    assertThat(professor).hasFieldOrPropertyWithValue("firstName", "Bob");
    assertThat(professor).hasFieldOrPropertyWithValue("lastName", "Martin");
    assertThat(professor.getInfo()).hasFieldOrPropertyWithValue("id", 1);
    assertThat(professor.getCourses().size()).isEqualTo(2);
    assertThat(professor.getCourses().get(0)).hasFieldOrPropertyWithValue("id", 1);
    assertThat(professor.getCourses().get(1)).hasFieldOrPropertyWithValue("id", 2);
    assertThat(professor.getLessons().size()).isEqualTo(2);
    assertThat(professor.getLessons().get(0)).hasFieldOrPropertyWithValue("id", 1);
    assertThat(professor.getLessons().get(1)).hasFieldOrPropertyWithValue("id", 3);
  }

  @Test
  void findById_shouldReturnEmptyOptional_whenProfessorDoesNotExist() {
    Optional<Professor> professor = repository.findById(3);
    assertThat(professor).isEmpty();
  }

  @Test
  void findAll_shouldReturnProfessorsPage_whenProfessorsExist() {
    List<Professor> professorList = repository.findAll(pageable).toList();

    assertThat(professorList.size()).isEqualTo(2);
    assertThat(professorList.get(0)).hasFieldOrPropertyWithValue("id", 1);
    assertThat(professorList.get(1)).hasFieldOrPropertyWithValue("id", 2);
  }

  @Test
  @Sql(
      value = {"/professor/cleanTestTables.sql"},
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  void findAll_shouldReturnEmptyPage_whenProfessorsDoNotExist() {
    Page<Professor> professors = repository.findAll(pageable);

    assertThat(professors.getTotalElements()).isZero();
  }

  @Test
  void save_shouldAddProfessor_whenProfessorIsNew() {
    Audit audit =
        Audit.builder()
            .createdBy("name")
            .createdDatetime(Instant.now())
            .updatedBy("name")
            .updatedDatetime(Instant.now())
            .build();
    PersonInfo info =
        PersonInfo.builder()
            .phoneNumber("11113")
            .email("email3@gmail")
            .address("address3")
            .audit(audit)
            .build();
    Professor professor =
        Professor.builder().firstName("John").lastName("Doe").info(info).audit(audit).build();

    Professor savedProfessor = repository.save(professor);
    List<Professor> updatedProfessors = repository.findAll(pageable).toList();

    assertThat(savedProfessor.getId()).isEqualTo(3);
    assertThat(savedProfessor.getInfo().getId()).isEqualTo(3);
    assertThat(updatedProfessors.size()).isEqualTo(3);
    assertThat(updatedProfessors.get(0).getId()).isEqualTo(1);
    assertThat(updatedProfessors.get(1).getId()).isEqualTo(2);
    assertThat(updatedProfessors.get(2).getId()).isEqualTo(3);
  }

  @Test
  void save_shouldUpdateProfessor_whenProfessorExists() {
    Professor professor = repository.findById(1).get();
    professor.setFirstName("John");
    repository.save(professor);

    Professor updatedProfessor = repository.findById(1).get();
    List<Professor> updatedProfessors = repository.findAll(pageable).toList();

    assertThat(updatedProfessor).hasFieldOrPropertyWithValue("firstName", "John");
    assertThat(updatedProfessor.getInfo()).hasFieldOrPropertyWithValue("id", 1);
    assertThat(updatedProfessors.size()).isEqualTo(2);
    assertThat(updatedProfessors.get(0).getId()).isEqualTo(1);
    assertThat(updatedProfessors.get(1).getId()).isEqualTo(2);
  }

  @Test
  void delete_shouldDeleteProfessor() {
    Professor professor = repository.findById(1).get();
    repository.delete(professor);

    Optional<Professor> deletedProfessor = repository.findById(1);
    List<Professor> updatedProfessors = repository.findAll(pageable).toList();

    assertThat(deletedProfessor).isEmpty();
    assertThat(updatedProfessors.size()).isEqualTo(1);
    assertThat(updatedProfessors.get(0).getId()).isEqualTo(2);
  }
}
