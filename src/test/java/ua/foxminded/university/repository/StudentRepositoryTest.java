/* (C)2023-2024 */
package ua.foxminded.university.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import ua.foxminded.university.repository.entity.Audit;
import ua.foxminded.university.repository.entity.Faculty;
import ua.foxminded.university.repository.entity.Group;
import ua.foxminded.university.repository.entity.PersonInfo;
import ua.foxminded.university.repository.entity.Student;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Sql(
    value = {"/student/populateTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(
    value = {"/student/cleanTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class StudentRepositoryTest {
  @Autowired StudentRepository repository;
  Pageable pageable = PageRequest.of(0, 10);

  @Test
  void findById_shouldReturnStudent_whenStudentExists() {
    Student student = repository.findById(2).get();

    assertThat(student).hasFieldOrPropertyWithValue("id", 2);
    assertThat(student).hasFieldOrPropertyWithValue("firstName", "Sam");
    assertThat(student).hasFieldOrPropertyWithValue("lastName", "Smith");
    assertThat(student).hasFieldOrPropertyWithValue("year", 1);
    assertThat(student.getInfo()).hasFieldOrPropertyWithValue("id", 2);
    assertThat(student.getGroup()).hasFieldOrPropertyWithValue("id", 1);
    assertThat(student.getFaculty()).hasFieldOrPropertyWithValue("id", 1);
  }

  @Test
  void findById_shouldReturnEmptyOptional_whenStudentDoesNotExist() {
    Optional<Student> student = repository.findById(5);
    assertThat(student).isEmpty();
  }

  @Test
  void findAll_shouldReturnStudentsPage_whenStudentsExist() {
    List<Student> studentList = repository.findAll(pageable).toList();

    assertThat(studentList.size()).isEqualTo(4);
    assertThat(studentList.get(0)).hasFieldOrPropertyWithValue("id", 1);
    assertThat(studentList.get(1)).hasFieldOrPropertyWithValue("id", 2);
    assertThat(studentList.get(2)).hasFieldOrPropertyWithValue("id", 3);
    assertThat(studentList.get(3)).hasFieldOrPropertyWithValue("id", 4);
  }

  @Test
  @Sql(
      value = {"/student/cleanTestTables.sql"},
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  void findAll_shouldReturnEmptyPage_whenStudentsDoNotExist() {
    Page<Student> students = repository.findAll(pageable);

    assertThat(students.getTotalElements()).isZero();
  }

  @Test
  void save_shouldAddStudent_whenStudentIsNew() {
    Audit audit =
        Audit.builder()
            .createdBy("name")
            .createdDatetime(Instant.now())
            .updatedBy("name")
            .updatedDatetime(Instant.now())
            .build();
    Faculty faculty =
        Faculty.builder().id(1).name("Business").description("Business description").build();
    Group group = Group.builder().id(1).name("B1").faculty(faculty).build();
    PersonInfo info =
        PersonInfo.builder()
            .phoneNumber("11115")
            .email("email5@.com")
            .address("address5")
            .audit(audit)
            .build();
    Student student =
        Student.builder()
            .group(group)
            .faculty(faculty)
            .year(1)
            .firstName("Harry")
            .lastName("Potter")
            .info(info)
            .audit(audit)
            .build();

    Student savedStudent = repository.save(student);
    List<Student> updatedStudents = repository.findAll(pageable).toList();

    assertThat(savedStudent.getId()).isEqualTo(5);
    assertThat(savedStudent.getInfo().getId()).isEqualTo(5);
    assertThat(savedStudent.getFaculty().getId()).isEqualTo(1);
    assertThat(savedStudent.getGroup().getId()).isEqualTo(1);
    assertThat(updatedStudents.size()).isEqualTo(5);
    assertThat(updatedStudents.get(0).getId()).isEqualTo(1);
    assertThat(updatedStudents.get(1).getId()).isEqualTo(2);
    assertThat(updatedStudents.get(2).getId()).isEqualTo(3);
    assertThat(updatedStudents.get(3).getId()).isEqualTo(4);
    assertThat(updatedStudents.get(4).getId()).isEqualTo(5);
  }

  @Test
  void save_shouldUpdateStudent_whenStudentExists() {
    Student student = repository.findById(1).get();
    student.setYear(3);
    repository.save(student);

    Student updatedStudent = repository.findById(1).get();
    List<Student> updatedStudents = repository.findAll(pageable).toList();

    assertThat(updatedStudent).hasFieldOrPropertyWithValue("year", 3);
    assertThat(updatedStudent.getInfo()).hasFieldOrPropertyWithValue("id", 1);
    assertThat(updatedStudents.size()).isEqualTo(4);
    assertThat(updatedStudents.get(0).getId()).isEqualTo(1);
    assertThat(updatedStudents.get(1).getId()).isEqualTo(2);
    assertThat(updatedStudents.get(2).getId()).isEqualTo(3);
    assertThat(updatedStudents.get(3).getId()).isEqualTo(4);
  }

  @Test
  void delete_shouldDeleteStudent_whenStudentExists() {
    Student student = repository.findById(2).get();
    repository.delete(student);

    Optional<Student> deletedStudent = repository.findById(2);
    List<Student> updatedStudents = repository.findAll(pageable).toList();

    assertThat(deletedStudent).isEmpty();
    assertThat(updatedStudents.size()).isEqualTo(3);
    assertThat(updatedStudents.get(0).getId()).isEqualTo(1);
    assertThat(updatedStudents.get(1).getId()).isEqualTo(3);
    assertThat(updatedStudents.get(2).getId()).isEqualTo(4);
  }
}
