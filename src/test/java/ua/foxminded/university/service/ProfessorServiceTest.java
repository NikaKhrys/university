/* (C)2024 */
package ua.foxminded.university.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.mappers.ProfessorMapper;
import ua.foxminded.university.repository.ProfessorRepository;
import ua.foxminded.university.repository.entity.Professor;
import ua.foxminded.university.service.dto.ProfessorDto;
import ua.foxminded.university.service.impls.ProfessorServiceImpl;

@ExtendWith(MockitoExtension.class)
class ProfessorServiceTest {
  @InjectMocks private ProfessorServiceImpl service;
  @Mock private ProfessorRepository repository;
  @Mock private ProfessorMapper mapper;
  private static final Pageable DEFAULT_PAGEABLE = PageRequest.of(0, 2);
  private static final String DEFAULT_USERNAME = "username";

  @Test
  void findAll_shouldReturnProfessorPage_whenProfessorsFound() {
    Page<Professor> foundProfessorsEntity = TestDataGenerator.getProfessorEntityPage();
    Page<ProfessorDto> foundProfessorsDto = TestDataGenerator.getProfessorDtoPage();
    when(repository.findAll(DEFAULT_PAGEABLE)).thenReturn(foundProfessorsEntity);
    when(mapper.toDtos(foundProfessorsEntity)).thenReturn(foundProfessorsDto);

    Page<ProfessorDto> result = service.findAll(DEFAULT_PAGEABLE);

    assertEquals(foundProfessorsDto, result);
    verify(repository, times(1)).findAll(DEFAULT_PAGEABLE);
  }

  @Test
  void findById_shouldReturnProfessor_whenProfessorExists() {
    Professor foundProfessorEntity = TestDataGenerator.getProfessorEntity(1);
    ProfessorDto foundProfessorDto = TestDataGenerator.getProfessorDto(1);
    when(repository.findById(1)).thenReturn(Optional.of(foundProfessorEntity));
    when(mapper.toDto(foundProfessorEntity)).thenReturn(foundProfessorDto);

    ProfessorDto result = service.findById(1);

    assertEquals(foundProfessorDto, result);
    verify(repository, times(1)).findById(1);
  }

  @Test
  void findById_shouldThrowException_whenProfessorDoesNotExist() {
    when(repository.findById(12)).thenThrow(RecordNotFoundException.class);

    assertThrows(RecordNotFoundException.class, () -> service.findById(12));
  }

  @Test
  void save_shouldReturnSavedProfessor_whenSaveIsSuccessful() {
    ProfessorDto professorToBeSavedDto = TestDataGenerator.getProfessorDto(null);
    Professor professorToBeSavedEntity = TestDataGenerator.getProfessorEntity(null);
    ProfessorDto savedProfessorDto = TestDataGenerator.getProfessorDto(1);
    Professor savedProfessorEntity = TestDataGenerator.getProfessorEntity(1);

    when(mapper.toEntity(professorToBeSavedDto, DEFAULT_USERNAME))
        .thenReturn(professorToBeSavedEntity);
    when(repository.save(professorToBeSavedEntity)).thenReturn(savedProfessorEntity);
    when(mapper.toDto(savedProfessorEntity)).thenReturn(savedProfessorDto);

    ProfessorDto result = service.save(professorToBeSavedDto, DEFAULT_USERNAME);

    assertEquals(savedProfessorDto, result);
    verify(repository, times(1)).save(professorToBeSavedEntity);
  }

  @Test
  void update_shouldReturnUpdatedProfessor_WhenProfessorExists() {
    ProfessorDto professorToBeUpdatedDto = TestDataGenerator.getProfessorDto(null);
    Professor professorToBeUpdatedEntity = TestDataGenerator.getProfessorEntity(null);
    ProfessorDto updatedProfessorDto = TestDataGenerator.getProfessorDto(1);
    Professor updatedProfessorEntity = TestDataGenerator.getProfessorEntity(1);

    when(repository.findById(1)).thenReturn(Optional.of(professorToBeUpdatedEntity));
    when(mapper.toEntity(professorToBeUpdatedEntity, professorToBeUpdatedDto, DEFAULT_USERNAME))
        .thenReturn(professorToBeUpdatedEntity);
    when(repository.save(professorToBeUpdatedEntity)).thenReturn(updatedProfessorEntity);
    when(mapper.toDto(updatedProfessorEntity)).thenReturn(updatedProfessorDto);

    ProfessorDto result = service.update(1, professorToBeUpdatedDto, DEFAULT_USERNAME);

    assertEquals(updatedProfessorDto, result);
    verify(repository, times(1)).findById(1);
    verify(repository, times(1)).save(professorToBeUpdatedEntity);
  }

  @Test
  void update_shouldThrowException_whenProfessorDoesNotExist() {
    ProfessorDto professorToBeUpdatedDto = TestDataGenerator.getProfessorDto(null);

    when(repository.findById(1)).thenThrow(RecordNotFoundException.class);

    assertThrows(
        RecordNotFoundException.class,
        () -> service.update(1, professorToBeUpdatedDto, DEFAULT_USERNAME));
    verify(repository, times(1)).findById(1);
  }

  @Test
  void deleteById_shouldDeleteProfessor_whenProfessorExists() {
    Professor existingProfessor = TestDataGenerator.getProfessorEntity(1);
    when(repository.findById(1)).thenReturn(Optional.of(existingProfessor));

    service.deleteById(1);

    verify(repository, times(1)).delete(existingProfessor);
  }

  @Test
  void deleteById_shouldThrowException_whenProfessorDoesNotExist() {
    when(repository.findById(1)).thenReturn(Optional.empty());

    assertThrows(RecordNotFoundException.class, () -> service.deleteById(1));
    verify(repository, times(1)).findById(1);
  }
}
