/* (C)2024 */
package ua.foxminded.university.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.exceptionhandler.exceptions.RecordNotFoundException;
import ua.foxminded.university.mappers.StudentMapper;
import ua.foxminded.university.repository.StudentRepository;
import ua.foxminded.university.repository.entity.Student;
import ua.foxminded.university.service.dto.StudentDto;
import ua.foxminded.university.service.impls.StudentServiceImpl;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {
  @InjectMocks private StudentServiceImpl service;
  @Mock private StudentRepository repository;
  @Mock private StudentMapper mapper;
  private static final Pageable DEFAULT_PAGEABLE = PageRequest.of(0, 2);
  private static final String DEFAULT_USERNAME = "username";

  @Test
  void findAll_shouldReturnStudentPage_whenStudentsFound() {
    Page<Student> foundStudentsEntity = TestDataGenerator.getStudentEntityPage();
    Page<StudentDto> foundStudentsDto = TestDataGenerator.getStudentDtoPage();
    when(repository.findAll(DEFAULT_PAGEABLE)).thenReturn(foundStudentsEntity);
    when(mapper.toDtos(foundStudentsEntity)).thenReturn(foundStudentsDto);

    Page<StudentDto> result = service.findAll(DEFAULT_PAGEABLE);

    assertEquals(foundStudentsDto, result);
    verify(repository, times(1)).findAll(DEFAULT_PAGEABLE);
  }

  @Test
  void findById_shouldReturnStudent_whenStudentExists() {
    Student foundStudentEntity = TestDataGenerator.getStudentEntity(1);
    StudentDto foundStudentDto = TestDataGenerator.getStudentDto(1);
    when(repository.findById(1)).thenReturn(Optional.of(foundStudentEntity));
    when(mapper.toDto(foundStudentEntity)).thenReturn(foundStudentDto);

    StudentDto result = service.findById(1);

    assertEquals(foundStudentDto, result);
    verify(repository, times(1)).findById(1);
  }

  @Test
  void findById_shouldThrowException_whenStudentDoesNotExist() {
    when(repository.findById(12)).thenThrow(RecordNotFoundException.class);

    assertThrows(RecordNotFoundException.class, () -> service.findById(12));
  }

  @Test
  void save_shouldReturnSavedStudent_whenSaveIsSuccessful() {
    StudentDto studentToBeSavedDto = TestDataGenerator.getStudentDto(null);
    Student studentToBeSavedEntity = TestDataGenerator.getStudentEntity(null);
    StudentDto savedStudentDto = TestDataGenerator.getStudentDto(1);
    Student savedStudentEntity = TestDataGenerator.getStudentEntity(1);

    when(mapper.toEntity(studentToBeSavedDto, DEFAULT_USERNAME)).thenReturn(studentToBeSavedEntity);
    when(repository.save(studentToBeSavedEntity)).thenReturn(savedStudentEntity);
    when(mapper.toDto(savedStudentEntity)).thenReturn(savedStudentDto);

    StudentDto result = service.save(studentToBeSavedDto, DEFAULT_USERNAME);

    assertEquals(savedStudentDto, result);
    verify(repository, times(1)).save(studentToBeSavedEntity);
  }

  @Test
  void update_shouldReturnUpdatedStudent_WhenStudentExists() {
    StudentDto studentToBeUpdatedDto = TestDataGenerator.getStudentDto(null);
    Student studentToBeUpdatedEntity = TestDataGenerator.getStudentEntity(null);
    StudentDto updatedStudentDto = TestDataGenerator.getStudentDto(1);
    Student updatedStudentEntity = TestDataGenerator.getStudentEntity(1);

    when(repository.findById(1)).thenReturn(Optional.of(studentToBeUpdatedEntity));
    when(mapper.toEntity(studentToBeUpdatedEntity, studentToBeUpdatedDto, DEFAULT_USERNAME))
        .thenReturn(studentToBeUpdatedEntity);
    when(repository.save(studentToBeUpdatedEntity)).thenReturn(updatedStudentEntity);
    when(mapper.toDto(updatedStudentEntity)).thenReturn(updatedStudentDto);

    StudentDto result = service.update(1, studentToBeUpdatedDto, DEFAULT_USERNAME);

    assertEquals(updatedStudentDto, result);
    verify(repository, times(1)).findById(1);
    verify(repository, times(1)).save(studentToBeUpdatedEntity);
  }

  @Test
  void update_shouldThrowException_whenUserDoesNotExist() {
    StudentDto studentToBeUpdatedDto = TestDataGenerator.getStudentDto(null);

    when(repository.findById(1)).thenThrow(RecordNotFoundException.class);

    assertThrows(
        RecordNotFoundException.class,
        () -> service.update(1, studentToBeUpdatedDto, DEFAULT_USERNAME));
    verify(repository, times(1)).findById(1);
  }

  @Test
  void deleteById_shouldDeleteUser_whenUserExists() {
    Student existingStudent = TestDataGenerator.getStudentEntity(1);
    when(repository.findById(1)).thenReturn(Optional.of(existingStudent));

    service.deleteById(1);

    verify(repository, times(1)).delete(existingStudent);
  }

  @Test
  void deleteById_shouldThrowException_whenUserDoesNotExist() {
    when(repository.findById(1)).thenReturn(Optional.empty());

    assertThrows(RecordNotFoundException.class, () -> service.deleteById(1));
    verify(repository, times(1)).findById(1);
  }
}
