/* (C)2024 */
package ua.foxminded.university.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ua.foxminded.university.repository.entity.Course;
import ua.foxminded.university.repository.entity.Faculty;
import ua.foxminded.university.repository.entity.Group;
import ua.foxminded.university.repository.entity.PersonInfo;
import ua.foxminded.university.repository.entity.Professor;
import ua.foxminded.university.repository.entity.Student;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.ProfessorDto;
import ua.foxminded.university.service.dto.StudentDto;

public class TestDataGenerator {
  private static final Pageable DEFAULT_PAGEABLE = PageRequest.of(0, 2);
  private static final int DEFAULT_TOTAL = 4;

  public static Student getStudentEntity(Integer id) {
    return Student.builder()
        .id(id)
        .year(1)
        .group(Group.builder().id(1).build())
        .faculty(Faculty.builder().id(1).build())
        .firstName("firstName")
        .lastName("lastName")
        .info(getPersonInfoEntity(id))
        .build();
  }

  public static StudentDto getStudentDto(Integer id) {
    return StudentDto.builder()
        .id(id)
        .year(1)
        .groupId(1)
        .facultyId(1)
        .firstName("firstName")
        .lastName("lastName")
        .info(getPersonInfoDto(id))
        .build();
  }

  public static Page<Student> getStudentEntityPage() {
    Student student1 = getStudentEntity(1);
    Student student2 = getStudentEntity(2);
    return new PageImpl<>(List.of(student1, student2), DEFAULT_PAGEABLE, DEFAULT_TOTAL);
  }

  public static Page<StudentDto> getStudentDtoPage() {
    StudentDto student1 = getStudentDto(1);
    StudentDto student2 = getStudentDto(2);
    return new PageImpl<>(List.of(student1, student2), DEFAULT_PAGEABLE, DEFAULT_TOTAL);
  }

  public static PersonInfo getPersonInfoEntity(Integer infoId) {
    return PersonInfo.builder()
        .id(infoId)
        .email("email")
        .address("address")
        .phoneNumber("380111111111")
        .build();
  }

  public static PersonInfoDto getPersonInfoDto(Integer infoId) {
    return PersonInfoDto.builder()
        .id(infoId)
        .email("email")
        .address("address")
        .phoneNumber("380111111111")
        .build();
  }

  public static Professor getProfessorEntity(Integer id) {
    return Professor.builder()
        .id(id)
        .courses(List.of(Course.builder().id(1).build(), Course.builder().id(2).build()))
        .firstName("firstName")
        .lastName("lastName")
        .info(getPersonInfoEntity(id))
        .build();
  }

  public static ProfessorDto getProfessorDto(Integer id) {
    return ProfessorDto.builder()
        .id(id)
        .coursesId(List.of(1, 2))
        .firstName("firstName")
        .lastName("lastName")
        .info(getPersonInfoDto(id))
        .build();
  }

  public static Page<Professor> getProfessorEntityPage() {
    Professor professor1 = getProfessorEntity(1);
    Professor professor2 = getProfessorEntity(2);

    return new PageImpl<>(List.of(professor1, professor2), DEFAULT_PAGEABLE, DEFAULT_TOTAL);
  }

  public static Page<ProfessorDto> getProfessorDtoPage() {
    ProfessorDto professor1 = getProfessorDto(1);
    ProfessorDto professor2 = getProfessorDto(2);

    return new PageImpl<>(List.of(professor1, professor2), DEFAULT_PAGEABLE, DEFAULT_TOTAL);
  }
}
