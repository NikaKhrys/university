INSERT INTO faculties (name,description) VALUES ('Business','Business description');

INSERT INTO groups (name,faculty_id) VALUES ('B1',1);
INSERT INTO groups (name,faculty_id) VALUES ('B2',1);

INSERT INTO persons_info (phone_number, email,address) VALUES (11111,'email1@.com','address1');
INSERT INTO persons_info (phone_number, email,address) VALUES (11112,'email2@.com','address2');
INSERT INTO persons_info (phone_number, email,address) VALUES (11113,'email3@.com','address3');
INSERT INTO persons_info (phone_number, email,address) VALUES (11114,'email4@.com','address4');

INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Milly','Smith',1,1,1,1);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Sam','Smith',1,1,1,2);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Tom','Lee',1,2,2,3);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Marta','Rodrigues',1,2,2,4);
