/* (C)2023 */
package ua.foxminded.university.controller;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.foxminded.university.UniversityApplication;

@Sql(
    value = {"/lesson/populateTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(
    value = {"/lesson/cleanTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UniversityApplication.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
@TestPropertySource(
    properties = {"dbName = testDb", "transactionsUrl=placeholder"},
    locations = "/application.properties")
class LessonControllerIntegrationTest {
  @Autowired private WebApplicationContext context;
  private MockMvc mvc;

  @BeforeEach
  public void mvcSetup() {
    this.mvc = MockMvcBuilders.webAppContextSetup(this.context).apply(springSecurity()).build();
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsAdmin()
      throws Exception {
    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2024-01-04")
                .param("page", "1")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content[0].date", is("2024-01-04")))
        .andExpect(jsonPath("$.content[0].time", is("12:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(1)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("B1")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("Bob")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("Logistics")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
  }

  @Test
  @WithMockUser(username = "millysmith")
  void
      getStudentSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsStudentWithAccess()
          throws Exception {
    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2024-01-04")
                .param("page", "1")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content[0].date", is("2024-01-04")))
        .andExpect(jsonPath("$.content[0].time", is("12:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(1)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("B1")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("Bob")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("Logistics")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
  }

  @Test
  @WithMockUser(username = "millysmith")
  void getStudentSchedule_shouldReturnUnauthorized_whenDateIsNotNullAndUserIsStudentWithoutAccess()
      throws Exception {
    mvc.perform(
            get("/lessons/student/2")
                .param("date", "2023-01-04")
                .param("page", "1")
                .param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void getStudentSchedule_shouldReturnUnauthorized_whenDateIsNotNullAndUserIsTeacher()
      throws Exception {
    mvc.perform(
            get("/lessons/student/2")
                .param("date", "2023-01-04")
                .param("page", "1")
                .param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnNoContent_whenDateIsNotNullAndPageIsEmpty() throws Exception {
    mvc.perform(
            get("/lessons/student/1")
                .param("date", "2023-05-07")
                .param("page", "0")
                .param("size", "5"))
        .andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnLessonsForMonth_whenMonthIsNotNull() throws Exception {
    mvc.perform(get("/lessons/student/2").param("month", "2").param("page", "0").param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].date", is("2024-02-10")))
        .andExpect(jsonPath("$.content[0].time", is("10:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(2)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("B1")))
        .andExpect(jsonPath("$.content[0].groups[1].name", is("B2")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("Sam")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("Math")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnNoContent_whenMonthIsNotNullAndPageIsEmpty()
      throws Exception {
    mvc.perform(get("/lessons/student/1").param("month", "5").param("page", "0").param("size", "2"))
        .andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNotNull() throws Exception {
    mvc.perform(
            get("/lessons/student/1")
                .param("month", "1")
                .param("date", "2023-01-01")
                .param("page", "1")
                .param("size", "1"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNull() throws Exception {
    mvc.perform(get("/lessons/student/1").param("page", "1").param("size", "1"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthIsNotPositive() throws Exception {
    mvc.perform(get("/lessons/student/1").param("page", "1").param("size", "1").param("month", "0"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getStudentSchedule_shouldReturnBadRequest_whenMonthIsGreaterThan12() throws Exception {
    mvc.perform(
            get("/lessons/student/1").param("page", "1").param("size", "1").param("month", "20"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsAdmin()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/4")
                .param("date", "2024-02-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].date", is("2024-02-10")))
        .andExpect(jsonPath("$.content[0].time", is("10:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(2)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("B1")))
        .andExpect(jsonPath("$.content[0].groups[1].name", is("B2")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("Sam")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("Math")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void getProfessorSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsTeacher()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/4")
                .param("date", "2024-02-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(1)))
        .andExpect(jsonPath("$.content[0].date", is("2024-02-10")))
        .andExpect(jsonPath("$.content[0].time", is("10:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(2)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("B1")))
        .andExpect(jsonPath("$.content[0].groups[1].name", is("B2")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("Sam")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("Math")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(2)));
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void getProfessorSchedule_shouldReturnLessonsPageForDay_whenDateIsNotNullAndUserIsStudent()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/4")
                .param("date", "2023-02-10")
                .param("page", "0")
                .param("size", "1"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnNoContent_whenDateIsNotNullAndPageIsEmpty()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/4")
                .param("date", "2023-01-01")
                .param("page", "0")
                .param("size", "2"))
        .andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnLessonPageForMonth_whenMonthIsNotNull() throws Exception {
    mvc.perform(
            get("/lessons/professor/3").param("month", "1").param("page", "1").param("size", "1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content[0].date", is("2024-01-04")))
        .andExpect(jsonPath("$.content[0].time", is("12:30:00")))
        .andExpect(jsonPath("$.content[0].groups", hasSize(1)))
        .andExpect(jsonPath("$.content[0].groups[0].name", is("B1")))
        .andExpect(jsonPath("$.content[0].professor.firstName", is("Bob")))
        .andExpect(jsonPath("$.content[0].auditorium.number", is(101)))
        .andExpect(jsonPath("$.content[0].course.name", is("Logistics")))
        .andExpect(jsonPath("$.last", is(false)))
        .andExpect(jsonPath("$.totalPages", is(3)))
        .andExpect(jsonPath("$.totalElements", is(3)));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnNoContent_whenMonthIsNotNullAndPageIsEmpty()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/4").param("month", "5").param("page", "0").param("size", "1"))
        .andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNotNull()
      throws Exception {
    mvc.perform(
            get("/lessons/professor/4")
                .param("month", "2")
                .param("date", "2023-02-01")
                .param("page", "1")
                .param("size", "2"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthAndDateAreBothNull() throws Exception {
    mvc.perform(get("/lessons/professor/1").param("page", "1").param("size", "1"))
        .andExpect(status().isBadRequest());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthIsNotPositive() throws Exception {
    mvc.perform(
            get("/lessons/professor/1").param("page", "1").param("size", "1").param("month", "0"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void getProfessorSchedule_shouldReturnBadRequest_whenMonthIsGreaterThan12() throws Exception {
    mvc.perform(
            get("/lessons/professor/1").param("page", "1").param("size", "1").param("month", "20"))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }
}
