/* (C)2023 */
package ua.foxminded.university.controller;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.foxminded.university.UniversityApplication;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.ProfessorDto;

@Sql(
    value = {"/professor/populateTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(
    value = {"/professor/cleanTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UniversityApplication.class})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
@TestPropertySource(
    properties = {"dbName = testDb", "transactionsUrl=placeholder"},
    locations = "/application.properties")
class ProfessorControllerIntegrationTest {
  @Autowired private WebApplicationContext context;
  private MockMvc mvc;

  @BeforeEach
  public void mvcSetup() {
    this.mvc = MockMvcBuilders.webAppContextSetup(this.context).apply(springSecurity()).build();
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnProfessorsPage_whenPageIsNotEmptyAndUserIsAdmin() throws Exception {
    mvc.perform(get("/professors").param("page", "0").param("size", "2"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].firstName", is("Bob")))
        .andExpect(jsonPath("$.content[1].firstName", is("Sam")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(1)))
        .andExpect(jsonPath("$.totalElements", is(2)));
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void findAll_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    mvc.perform(get("/professors").param("page", "0").param("size", "2"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnNoContent_whenPageIsEmpty() throws Exception {
    mvc.perform(get("/professors").param("page", "1").param("size", "2"))
        .andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnProfessor_whenProfessorExistsAndUserIsAdmin() throws Exception {
    mvc.perform(get("/professors/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("Bob")))
        .andExpect(jsonPath("$.lastName", is("Martin")))
        .andExpect(jsonPath("$.coursesId", hasSize(2)))
        .andExpect(jsonPath("$.coursesId[0]", is(1)))
        .andExpect(jsonPath("$.coursesId[1]", is(2)))
        .andExpect(jsonPath("$.info.email", is("email1@.com")));
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void findById_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    mvc.perform(get("/professors/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnErrorMessage_whenProfessorDoesNotExist() throws Exception {
    mvc.perform(get("/professors/4"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", is(anything())))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 4 wasn't found")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void save_shouldReturnProfessorWithId_whenProfessorIsNewAndUserIsAdmin() throws Exception {
    ProfessorDto professor = getTestData();
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(post("/professors").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(3)))
        .andExpect(jsonPath("$.firstName", is("Harry")));
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void save_shouldReturnProfessorWithId_whenUserIsUnauthorized() throws Exception {
    ProfessorDto professor = getTestData();
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(post("/professors").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(3)))
        .andExpect(jsonPath("$.firstName", is("Harry")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void save_shouldReturnBadRequest_whenProfessorIsNotValid() throws Exception {
    ProfessorDto professor = getTestData();
    professor.setCoursesId(null);
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(post("/professors").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnUpdatedProfessor_whenProfessorExistsAndUserIsAdmin() throws Exception {
    ProfessorDto professor = getTestData();
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/2").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(2)))
        .andExpect(jsonPath("$.firstName", is("Harry")));
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void update_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    ProfessorDto professor = getTestData();
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/2").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnErrorMessage_whenProfessorDoesNotExist() throws Exception {
    ProfessorDto professor = getTestData();
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/4").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 4 wasn't found")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnBadRequest_whenProfessorIsNotValid() throws Exception {
    ProfessorDto professor = getTestData();
    professor.setFirstName("     ");
    String jsonProfessor = new ObjectMapper().writeValueAsString(professor);

    mvc.perform(put("/professors/1").contentType(MediaType.APPLICATION_JSON).content(jsonProfessor))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnNoContent_whenProfessorExistsAndUserIsAdmin() throws Exception {
    mvc.perform(delete("/professors/1")).andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void deleteById_shouldReturnNoContent_whenUserIsNotAdmin() throws Exception {
    mvc.perform(delete("/professors/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnErrorMessage_whenProfessorDoesNotExist() throws Exception {
    mvc.perform(delete("/professors/5"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 5 wasn't found")));
  }

  private ProfessorDto getTestData() {
    PersonInfoDto personInfo =
        PersonInfoDto.builder()
            .email("newEmail@gmail.com")
            .address("newAddress")
            .phoneNumber("380555555555")
            .build();

    return ProfessorDto.builder()
        .coursesId(List.of(1))
        .firstName("Harry")
        .lastName("Potter")
        .info(personInfo)
        .build();
  }
}
