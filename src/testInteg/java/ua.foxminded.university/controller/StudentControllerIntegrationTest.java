/* (C)2023 */
package ua.foxminded.university.controller;

import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.foxminded.university.UniversityApplication;
import ua.foxminded.university.service.dto.PersonInfoDto;
import ua.foxminded.university.service.dto.StudentDto;

@Sql(
    value = {"/student/populateTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(
    value = {"/student/cleanTestTables.sql"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UniversityApplication.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@WebAppConfiguration
@TestPropertySource(
    properties = {"dbName = testDb", "transactionsUrl=placeholder"},
    locations = "/application.properties")
class StudentControllerIntegrationTest {
  @Autowired private WebApplicationContext context;
  private MockMvc mvc;

  @BeforeEach
  public void mvcSetup() {
    this.mvc = MockMvcBuilders.webAppContextSetup(this.context).apply(springSecurity()).build();
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnStudentsPage_whenPageIsNotEmptyAndUserIsAdmin() throws Exception {
    mvc.perform(get("/students").param("page", "1").param("size", "2"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].firstName", is("Tom")))
        .andExpect(jsonPath("$.content[1].firstName", is("Marta")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(4)));
  }

  @Test
  @WithMockUser(authorities = "PROFESSOR")
  void findAll_shouldReturnStudentsPage_whenPageIsNotEmptyAndUserIsTeacher() throws Exception {
    mvc.perform(get("/students").param("page", "1").param("size", "2"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].firstName", is("Tom")))
        .andExpect(jsonPath("$.content[1].firstName", is("Marta")))
        .andExpect(jsonPath("$.last", is(true)))
        .andExpect(jsonPath("$.totalPages", is(2)))
        .andExpect(jsonPath("$.totalElements", is(4)));
  }

  @Test
  @WithMockUser(authorities = "STUDENT")
  void findAll_shouldReturnUnauthorized_whenUserIsStudent() throws Exception {
    mvc.perform(get("/students").param("page", "1").param("size", "2"))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findAll_shouldReturnNoContent_whenPageIsEmpty() throws Exception {
    mvc.perform(get("/students").param("page", "4").param("size", "5"))
        .andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnStudent_whenStudentExistsAndUserIsAdmin() throws Exception {
    mvc.perform(get("/students/1"))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.firstName", is("Milly")))
        .andExpect(jsonPath("$.lastName", is("Smith")))
        .andExpect(jsonPath("$.year", is(1)))
        .andExpect(jsonPath("$.groupId", is(1)))
        .andExpect(jsonPath("$.facultyId", is(1)))
        .andExpect(jsonPath("$.info.email", is("email1@.com")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void findById_shouldReturnErrorMessage_whenStudentDoesNotExist() throws Exception {
    mvc.perform(get("/students/5"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 5 wasn't found")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void save_shouldReturnStudentWithId_whenStudentIsNewAndUserIsAdmin() throws Exception {
    StudentDto student = getTestData();
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(post("/students").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isCreated())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(5)))
        .andExpect(jsonPath("$.firstName", is("Harry")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void save_shouldReturnBadRequest_whenStudentIsNotValid() throws Exception {
    StudentDto student = getTestData();
    student.setYear(8);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(post("/students").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", anything()));
  }

  @Test
  void save_shouldReturnsUnauthorized_whenUserIsUnauthorized() throws Exception {
    StudentDto student = getTestData();
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(post("/students").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnUpdatedStudent_whenStudentExistsAndUserIsAdmin() throws Exception {
    StudentDto student = getTestData();
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/4").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isOk())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(4)))
        .andExpect(jsonPath("$.lastName", is("Potter")));
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void update_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    StudentDto student = getTestData();
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/4").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnErrorMessage_whenStudentDoesNotExist() throws Exception {
    StudentDto student = getTestData();
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/7").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 7 wasn't found")));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void update_shouldReturnBadRequest_whenStudentIsNotValid() throws Exception {
    StudentDto student = getTestData();
    student.setInfo(null);
    String jsonStudent = new ObjectMapper().writeValueAsString(student);

    mvc.perform(put("/students/1").contentType(MediaType.APPLICATION_JSON).content(jsonStudent))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("400 BAD_REQUEST")))
        .andExpect(jsonPath("$.error", is(anything())));
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteId_shouldReturnNoContent_whenStudentExistsAndUserIsAdmin() throws Exception {
    mvc.perform(delete("/students/1")).andExpect(status().isNoContent());
  }

  @Test
  @WithMockUser(authorities = {"PROFESSOR", "STUDENT"})
  void deleteId_shouldReturnUnauthorized_whenUserIsNotAdmin() throws Exception {
    mvc.perform(delete("/students/1")).andExpect(status().isUnauthorized());
  }

  @Test
  @WithMockUser(authorities = "ADMIN")
  void deleteById_shouldReturnErrorMessage_whenStudentDoesNotExist() throws Exception {
    mvc.perform(delete("/students/5"))
        .andExpect(status().isNotFound())
        .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.timestamp", anything()))
        .andExpect(jsonPath("$.code", is("404 NOT_FOUND")))
        .andExpect(jsonPath("$.error", is("Record 5 wasn't found")));
  }

  private StudentDto getTestData() {
    PersonInfoDto personInfo =
        PersonInfoDto.builder()
            .email("newEmail@gmail.com")
            .address("newAddress")
            .phoneNumber("380555555555")
            .build();

    return StudentDto.builder()
        .year(3)
        .groupId(2)
        .facultyId(1)
        .firstName("Harry")
        .lastName("Potter")
        .info(personInfo)
        .build();
  }
}
