INSERT INTO auditoriums (auditorium_number) VALUES (101);

INSERT INTO faculties (name,description) VALUES ('Business','Business description');

INSERT INTO groups (name,faculty_id) VALUES ('B1',1);
INSERT INTO groups (name,faculty_id) VALUES ('B2',1);

INSERT INTO persons_info (phone_number, email,address) VALUES (11111,'email1@.com','address1');
INSERT INTO persons_info (phone_number, email,address) VALUES (11112,'email2@.com','address2');
INSERT INTO persons_info (phone_number, email,address) VALUES (11113,'email3@.com','address3');
INSERT INTO persons_info (phone_number, email,address) VALUES (11114,'email4@.com','address4');

INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Milly','Smith',1,1,1,1);
INSERT INTO students (first_name,last_name,faculty_id,student_year,group_id,person_info_id) VALUES ('Sam','Smith',1,1,2,2);

INSERT INTO professors (first_name,last_name,person_info_id) VALUES ('Bob','Martin',3);
INSERT INTO professors (first_name,last_name,person_info_id) VALUES ('Sam','Lee',4);

INSERT INTO courses (name,description) VALUES ('Math','Math description');
INSERT INTO courses (name,description) VALUES ('Logistics','Logistics description');

INSERT INTO professors_courses (professor_id,course_id) VALUES (3,1);
INSERT INTO professors_courses (professor_id,course_id) VALUES (3,2);
INSERT INTO professors_courses (professor_id,course_id) VALUES (4,1);


INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-01','08:30:00',1,1,3);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-04','10:30:00',1,1,4);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-04','12:30:00',1,2,3);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-07','08:30:00',1,2,3);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-10','10:30:00',1,1,4);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-02-10','13:30:00',1,1,4);

INSERT INTO lessons_groups (lesson_id,group_id) VALUES (1,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (2,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (2,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (3,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (4,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (5,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (5,2);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (6,1);
INSERT INTO lessons_groups (lesson_id,group_id) VALUES (6,2);

INSERT INTO users (person_id,login,password,role) VALUES(7,'bobmartin','$2a$10$STsUV1gWULfZjXFw6r7UZeTSTwTc9zA0nRhsHcjooRbjEA7zDeSWW','ADMIN');
INSERT INTO users (person_id,login,password,role) VALUES(1,'millysmith','$2a$10$fBaLOUkszMbCpc5vRcBYQuCBrRWPvyy957r1vXE6IpmTftQmOAKR2','STUDENT');
INSERT INTO users (person_id,login,password,role) VALUES(8,'samlee','$2a$10$smaizNpAQKZ3uooVuhkZCuaA/35uZsFWvMaedkAe5v/4ec6LxV/aK','TEACHER');
