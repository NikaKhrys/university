INSERT INTO persons_info (phone_number, email,address) VALUES (11111,'email1@.com','address1');
INSERT INTO persons_info (phone_number, email,address) VALUES (11112,'email2@.com','address2');

INSERT INTO professors (first_name,last_name,person_info_id) VALUES ('Bob','Martin',1);
INSERT INTO professors (first_name,last_name,person_info_id) VALUES ('Sam','Lee',2);

INSERT INTO courses (name,description) VALUES ('Math','Math description');
INSERT INTO courses (name,description) VALUES ('Logistics','Logistics description');

INSERT INTO professors_courses (professor_id,course_id) VALUES (1,1);
INSERT INTO professors_courses (professor_id,course_id) VALUES (1,2);
INSERT INTO professors_courses (professor_id,course_id) VALUES (2,1);

INSERT INTO auditoriums (auditorium_number) VALUES (101);

INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-01','08:30:00',1,1,1);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-04','10:30:00',1,1,2);
INSERT INTO lessons (lesson_date,lesson_time,auditorium_id,course_id,professor_id) VALUES ('2024-01-04','12:30:00',1,2,1);